{ pkgs ? import <nixpkgs> {}
, stdenv ? pkgs.stdenv
, ...
}:

stdenv.mkDerivation rec {
  pname = "trainsim";
  version = "0.0.1";

  buildInputs = with pkgs; [
    freetype
    xorg.libX11
    xorg.libXcursor
    xorg.libXrandr
    xorg.libXi
    wayland
    wayland-protocols
    libGL
    python3
  ];

  shellHook = ''
    export LD_LIBRARY_PATH="${stdenv.lib.makeLibraryPath buildInputs}:$LD_LIBRARY_PATH"
  '';
}
