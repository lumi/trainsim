use {
    cgmath::{prelude::*, Point3, Vector3},
    generational_arena::{Arena, Index as GenIndex},
    specs::prelude::*,
    std::{
        cmp::Ordering,
        collections::{BinaryHeap, HashMap},
    },
};

use crate::{
    components::{Rail, Junction, Station, HasStation},
    config::MIN_TURN_DOT,
    track::TrackPos,
};

#[derive(Debug, Clone)]
pub struct JNode {
    pub entity: Entity,
    pub name: String,
    pub point: Point3<f32>,
    pub exits: Vec<Exit>,
}

#[derive(Debug, Clone, Copy)]
pub struct Exit {
    pub entity: Entity,
    pub entry_dir: Vector3<f32>,
    pub exit_dir: Vector3<f32>,
    pub dist: f32,
    pub index: GenIndex,
    pub signal: bool,
}

#[derive(Debug, Clone, Copy)]
pub struct RNode {
    pub point: Point3<f32>,
    pub index: GenIndex,
}

pub struct LinkGraph {
    pub jnodes: Arena<JNode>,
    pub jmap: HashMap<Entity, GenIndex>,
    pub stations: HashMap<String, Entity>,
    pub dirty: bool,
}

impl Default for LinkGraph {
    fn default() -> LinkGraph {
        LinkGraph {
            jnodes: Arena::new(),
            jmap: HashMap::new(),
            stations: HashMap::new(),
            dirty: true,
        }
    }
}

impl LinkGraph {
    pub fn clear(&mut self) {
        self.jnodes.clear();
        self.jmap.clear();
        self.stations.clear();
        self.dirty = true;
    }

    fn add_jnode(&mut self, entity: Entity, name: impl Into<String>, point: Point3<f32>) -> GenIndex {
        let jidx = self.jnodes.insert(JNode {
            entity,
            name: name.into(),
            point,
            exits: Vec::new(),
        });
        self.jmap.insert(entity, jidx);
        jidx
    }

    fn with_jnode<F: FnOnce(&mut JNode)>(&mut self, idx: GenIndex, func: F) {
        if let Some(jnode) = self.jnodes.get_mut(idx) {
            func(jnode);
        }
        else {
            panic!("junction not found");
        }
    }

    pub fn get_jnode(&self, ent: Entity) -> Option<&JNode> {
        self.jmap.get(&ent).and_then(|&jidx| self.jnodes.get(jidx))
    }

    fn connect(&mut self, entity: Entity, start: Entity, end: Entity, dist: f32, entry_dir: Vector3<f32>, exit_dir: Vector3<f32>, oneway: bool, signal: bool) {
        let start_jidx = *self.jmap.get(&start).unwrap();
        let end_jidx = *self.jmap.get(&end).unwrap();
        self.with_jnode(start_jidx, |j| {
            j.exits.push(Exit {
                entity,
                entry_dir, exit_dir,
                dist,
                index: end_jidx,
                signal,
            });
        });
        if !oneway {
            self.with_jnode(end_jidx, |j| {
                j.exits.push(Exit {
                    entity,
                    entry_dir: -exit_dir,
                    exit_dir: -entry_dir,
                    dist,
                    index: start_jidx,
                    signal,
                });
            });
        }
    }

    pub fn find_path<FG, FR, FP>(
        &self,
        start_rail: &Rail,
        start_pos: TrackPos,
        dir: Vector3<f32>,
        mut is_goal: FG,
        mut is_reserved: FR,
        mut get_penalty: FP,
    ) -> Option<Vec<Entity>>
    where
        FG: FnMut(Entity) -> bool,
        FR: FnMut(Entity) -> bool,
        FP: FnMut(Entity) -> f32,
    {
        if is_goal(start_pos.entity) {
            log::trace!("already on goal!");
            return Some(Vec::new());
        }
        else {
            log::trace!("not on goal, must find a path");
        }
        #[derive(Debug)]
        struct Item {
            dist: f32,
            index: GenIndex,
            entity: Entity,
            dir: Vector3<f32>,
            speculative: Option<Link>,
        }
        impl PartialEq<Item> for Item {
            fn eq(&self, other: &Item) -> bool {
                self.dist == other.dist
            }
        }
        impl Eq for Item {}
        impl PartialOrd<Item> for Item {
            fn partial_cmp(&self, other: &Item) -> Option<Ordering> {
                other.dist.partial_cmp(&self.dist)
            }
        }
        impl Ord for Item {
            fn cmp(&self, other: &Item) -> Ordering {
                self.partial_cmp(other).unwrap()
            }
        }
        #[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
        struct Link(GenIndex, Entity);
        let dir_along_track = start_rail.tangent_at(start_pos.t).normalize();
        log::trace!("direction along track: {:?}", dir_along_track);
        log::trace!("train direction: {:?}", dir);
        let (start_jidx, start_exit_dir) = if dir_along_track.dot(dir) > 0. {
            log::trace!("moving towards end: {:?} {:?}", start_rail.bezier.end, start_rail.end_dir);
            let jidx = *self.jmap.get(&start_rail.end_junction).unwrap();
            (jidx, start_rail.end_dir)
        } else {
            log::trace!("moving towards start: {:?} {:?}", start_rail.bezier.start, -start_rail.start_dir);
            let jidx = *self.jmap.get(&start_rail.start_junction).unwrap();
            (jidx, -start_rail.start_dir)
        };
        let mut visited: HashMap<Link, Link> = HashMap::new();
        let mut heap: BinaryHeap<Item> = BinaryHeap::new();
        heap.push(Item {
            dist: 0., // TODO: add distance from the train to the exit position
            index: start_jidx,
            entity: start_pos.entity,
            dir: start_exit_dir,
            speculative: None,
        });
        let start_link = Link(start_jidx, start_pos.entity);
        log::trace!("pathfinding start: {:?}", start_link);
        visited.insert(start_link, start_link);
        let mut result = None;
        while let Some(item) = heap.pop() {
            let link = Link(item.index, item.entity);
            if is_goal(item.entity) {
                result = item.speculative.or_else(|| Some(link));
                log::trace!("goal reached!");
                break;
            }
            let jnode = self.jnodes.get(item.index).unwrap();
            log::trace!("getting exits of {:?}", jnode.name);
            log::trace!("exit dir: {:?}", item.dir);
            for exit in &jnode.exits {
                let next_jnode = self.jnodes.get(exit.index).unwrap();
                log::trace!("exit found towards {:?}", next_jnode.name);
                log::trace!("exit entry dir: {:?}", exit.entry_dir);
                log::trace!("exit dot: {}", exit.entry_dir.dot(item.dir));
                if exit.entry_dir.dot(item.dir) < MIN_TURN_DOT {
                    log::trace!("turning angle too large");
                    continue;
                }
                let mut penalty = 0.;
                if is_reserved(exit.entity) {
                    if item.speculative.is_none() {
                        log::trace!("reserved track piece, not speculative");
                        continue;
                    } else {
                        penalty += 100.;
                    }
                }
                penalty += get_penalty(exit.entity);
                let new_link = Link(exit.index, exit.entity);
                if visited.contains_key(&new_link) {
                    continue;
                }
                visited.insert(new_link, link);
                let speculative = if item.speculative.is_some() {
                    item.speculative
                } else if exit.signal {
                    Some(new_link)
                } else {
                    None
                };
                heap.push(Item {
                    dist: item.dist + exit.dist + penalty,
                    index: exit.index,
                    entity: exit.entity,
                    dir: exit.exit_dir,
                    speculative,
                });
            }
        }
        if let Some(link) = result {
            let mut path = Vec::new();
            let mut cur = link;
            while cur != start_link {
                path.push(cur.1);
                cur = visited[&cur];
            }
            path.reverse();
            log::trace!("found path: {:?}", path);
            Some(path)
        } else {
            log::trace!("no path found!");
            None
        }
    }

    pub fn add_station(&mut self, name: &str, entity: Entity) {
        self.stations.insert(name.to_owned(), entity);
    }

    pub fn get_station(&self, name: &str) -> Option<Entity> {
        self.stations.get(name).cloned()
    }

    pub fn stations<'a>(&'a self) -> impl Iterator<Item = Entity> + 'a {
        self.stations.values().cloned()
    }
}

pub struct LinkGraphSystem;

impl<'a> System<'a> for LinkGraphSystem {
    type SystemData = (
        Write<'a, LinkGraph>,
        Entities<'a>,
        ReadStorage<'a, Rail>,
        ReadStorage<'a, Junction>,
        ReadStorage<'a, HasStation>,
        WriteStorage<'a, Station>,
    );

    fn run(&mut self, (mut lg, ents, rails, junctions, hss, mut stations): Self::SystemData) {
        if lg.dirty {
            log::info!("dirty linkgraph, recomputing");
            lg.clear();
            for (ent, station) in (&ents, &mut stations).join() {
                lg.add_station(&station.name, ent);
                station.rails.clear();
            }
            for (ent, hs) in (&ents, &hss).join() {
                if let Some(station) = stations.get_mut(hs.station) {
                    station.rails.push(ent);
                }
            }
            for (ent, junc) in (&ents, &junctions).join() {
                log::trace!("junction {:?} added at {:?}", junc.name, junc.pos);
                lg.add_jnode(ent, &junc.name, junc.pos);
            }
            for (ent, rail) in (&ents, &rails).join() {
                let start_ent = rail.start_junction;
                let end_ent = rail.end_junction;
                let start = rail.bezier.start;
                let end = rail.bezier.end;
                let dist = rail.dist;
                log::trace!("conduit added: {:?} {:?} {:?} {} {:?} {:?} {} {}", ent, start, end, dist, rail.entry_dir(), rail.exit_dir(), rail.oneway, rail.signal);
                lg.connect(
                    ent,
                    start_ent,
                    end_ent,
                    dist,
                    rail.entry_dir(),
                    rail.exit_dir(),
                    rail.oneway,
                    rail.signal,
                );
            }
            lg.dirty = false;
        }
    }
}
