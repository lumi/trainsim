#[derive(Debug, Clone, Copy, PartialEq, PartialOrd, Eq, Ord, Hash)]
pub enum TrainAiState {
    Arriving,
    Idle,
    Moving,
}
