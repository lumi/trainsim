use crate::passenger::Passenger;

#[derive(Debug, Clone)]
pub struct Wagon {
    pub offset: f32,
    pub passengers: Vec<Passenger>,
    pub capacity: usize,
}

impl Wagon {
    pub fn new(offset: f32, capacity: usize) -> Wagon {
        Wagon {
            offset,
            capacity,
            passengers: Vec::new(),
        }
    }
}
