use {
    cgmath::Point2,
    std::collections::HashSet,
};

#[cfg(feature = "graphics")]
use {
    glium::glutin::MouseButton,
};

#[cfg(not(feature = "graphics"))]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum MouseButton {}

#[cfg(feature = "graphics")]
mod system;
#[cfg(feature = "graphics")]
pub use system::InputSystem;

#[derive(Debug, Clone)]
pub struct Cursor {
    pub screen_pos: Point2<f32>,
    pub down: HashSet<MouseButton>,
    pub pressed: Vec<MouseButton>,
    pub scroll: f32,
}

impl Default for Cursor {
    fn default() -> Cursor {
        Cursor {
            screen_pos: Point2::new(0., 0.),
            down: HashSet::new(),
            pressed: Vec::new(),
            scroll: 0.,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Keyboard {
    pub down: HashSet<u32>,
    pub pressed: Vec<u32>,
}

impl Default for Keyboard {
    fn default() -> Keyboard {
        Keyboard {
            down: HashSet::new(),
            pressed: Vec::new(),
        }
    }
}
