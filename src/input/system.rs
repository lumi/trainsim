use {
    super::{Cursor, Keyboard},
    cgmath::Point2,
    glium::glutin,
    shrev::EventChannel,
    specs::prelude::*,
};

use crate::{
    camera::Camera,
};

pub struct InputSystem {
    events_reader_id: ReaderId<glutin::Event>,
}

impl InputSystem {
    pub fn new(events_reader_id: ReaderId<glutin::Event>) -> InputSystem {
        InputSystem { events_reader_id }
    }
}

impl<'a> System<'a> for InputSystem {
    type SystemData = (
        Read<'a, Camera>,
        Write<'a, Cursor>,
        Write<'a, Keyboard>,
        Read<'a, EventChannel<glutin::Event>>,
    );

    fn run(&mut self, (_cam, mut cs, mut kb, glutin_events): Self::SystemData) {
        kb.pressed.clear();
        cs.pressed.clear();
        cs.scroll = 0.;
        for evt in glutin_events.read(&mut self.events_reader_id) {
            match evt {
                glutin::Event::WindowEvent { event, .. } => match event {
                    glutin::WindowEvent::MouseInput { state, button, ..} => match state {
                        glutin::ElementState::Pressed => {
                            cs.pressed.push(*button);
                            cs.down.insert(*button);
                        },
                        glutin::ElementState::Released => {
                            cs.down.remove(button);
                        },
                    },
                    glutin::WindowEvent::MouseWheel { delta, ..} => {
                        match delta {
                            glutin::MouseScrollDelta::LineDelta(_x, y) => { cs.scroll += y; },
                            _ => (),
                        }
                    },
                    glutin::WindowEvent::CursorMoved { position, .. } => {
                        cs.screen_pos = Point2::new(position.x as f32, position.y as f32);
                    }
                    glutin::WindowEvent::KeyboardInput { input, .. } => match input.state {
                        glutin::ElementState::Pressed => {
                            kb.pressed.push(input.scancode);
                            kb.down.insert(input.scancode);
                        }
                        glutin::ElementState::Released => {
                            kb.down.remove(&input.scancode);
                        }
                    },
                    _ => (),
                },
                _ => (),
            }
        }
    }
}
