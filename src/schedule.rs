use {specs::prelude::*, std::collections::HashMap};

use crate::{
    color::Color,
    time::{Span, Time},
};

#[derive(Debug, Clone)]
pub struct Schedule {
    pub name: String,
    pub color: Color,
    pub stops: Vec<Stop>,
    pub every: Span,
    pub wagons: usize,
    pub slots: Vec<Slot>,
}

#[derive(Debug, Clone)]
pub struct Slot {
    pub id: usize,
    pub start: Time,
    pub train: Option<Entity>,
}

#[derive(Debug, Clone)]
pub struct Stop {
    pub name: String,
    pub offset: Span,
}

#[derive(Debug, Clone, Default)]
pub struct ScheduleDirectory {
    pub schedules: HashMap<String, Schedule>,
}
