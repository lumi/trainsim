use {
    std::{
        path::Path,
    },
    cgmath::{
        prelude::*, Point2, Point3, Vector3
    },
    glium::{
        self,
        index::PrimitiveType, IndexBuffer, VertexBuffer, backend::Facade,
    },
};

use crate::{
    color::Color,
    heightmap::HeightMap,
};

use super::{
    vertex::Vertex,
};

#[derive(Debug, Clone)]
pub struct Model<V: glium::Vertex = Vertex> {
    pub vertices: Vec<V>,
    pub indices: Vec<u32>,
}

impl<V: glium::Vertex> Model<V> {
    #[allow(dead_code)]
    pub fn new() -> Model<V> {
        Model {
            vertices: Vec::new(),
            indices: Vec::new(),
        }
    }

    pub fn to_gpu_model<F: Facade>(&self, facade: &F) -> GpuModel<V> {
        let vbo = VertexBuffer::new(facade, &self.vertices).unwrap();
        let ibo = IndexBuffer::new(facade, PrimitiveType::TrianglesList, &self.indices).unwrap();
        GpuModel {
            vbo,
            ibo,
        }
    }

    #[allow(dead_code)]
    pub fn merge(&mut self, other: &Model<V>) {
        let len = self.vertices.len() as u32;
        self.vertices.extend(&other.vertices);
        self.indices.extend(other.indices.iter().map(|&i| i + len));
    }
}

impl Model<Vertex> {
    pub fn load_from_obj(path: impl AsRef<Path>, name: &str) -> anyhow::Result<Model<Vertex>> {
        let path = path.as_ref();
        let mut obj: obj::Obj<obj::SimplePolygon> = obj::Obj::load(path)?;
        obj.load_mtls().map_err(|es| anyhow::anyhow!("errors occured while loading materials: {:?}", es))?;
        for o in &obj.objects {
            if o.name == name {
                let mut vertices = Vec::new();
                let mut indices = Vec::new();
                let mut last_idx = 0;
                for group in &o.groups {
                    let mut color = group.material.as_ref().and_then(|m| m.kd).map(|c| Color::from(c)).unwrap_or(Color::new(0., 0., 0., 1.));
                    if let Some(d) = group.material.as_ref().and_then(|m| m.d) {
                        color.alpha = d;
                    }
                    for p in &group.polys {
                        vertices.extend(p.into_iter().map(|t| {
                            let pos = Point3::from(obj.position[t.0]);
                            let uv = Point2::from(obj.texture[t.1.unwrap()]);
                            let n = Vector3::from(obj.normal[t.2.unwrap()]);
                            Vertex::new(pos, uv, n, color)
                        }));
                        indices.extend(&[
                            last_idx + 0,
                            last_idx + 1,
                            last_idx + 2,
                        ]);
                        last_idx += 3;
                    }
                }
                return Ok(Model {
                    vertices,
                    indices,
                });
            }
        }
        Err(anyhow::anyhow!("could not find object {:?} in {:?}", name, path))
    }

    pub fn bar_at(pos: Point3<f32>, radius: Vector3<f32>, color: Color) -> Model<Vertex> {
        let r = radius;
        let make_vertex = |pos: Point3<f32>, uv: Point2<f32>, normal: Vector3<f32>, color: Color| Vertex::new(pos, Point2::new(uv.x / 3., uv.y / 4.), normal, color);
        let vertices = vec![
            // x+
            make_vertex(pos + Vector3::new( r.x,  r.y,  r.z), Point2::new(2., 2.), Vector3::new( 1.,  0.,  0.), color),
            make_vertex(pos + Vector3::new( r.x, -r.y,  r.z), Point2::new(2., 1.), Vector3::new( 1.,  0.,  0.), color),
            make_vertex(pos + Vector3::new( r.x, -r.y, -r.z), Point2::new(3., 1.), Vector3::new( 1.,  0.,  0.), color),
            make_vertex(pos + Vector3::new( r.x,  r.y, -r.z), Point2::new(3., 2.), Vector3::new( 1.,  0.,  0.), color),

            // x-
            make_vertex(pos + Vector3::new(-r.x,  r.y,  r.z), Point2::new(1., 2.), Vector3::new(-1.,  0.,  0.), color),
            make_vertex(pos + Vector3::new(-r.x,  r.y, -r.z), Point2::new(0., 2.), Vector3::new(-1.,  0.,  0.), color),
            make_vertex(pos + Vector3::new(-r.x, -r.y, -r.z), Point2::new(0., 1.), Vector3::new(-1.,  0.,  0.), color),
            make_vertex(pos + Vector3::new(-r.x, -r.y,  r.z), Point2::new(1., 1.), Vector3::new(-1.,  0.,  0.), color),

            // y+
            make_vertex(pos + Vector3::new( r.x,  r.y,  r.z), Point2::new(2., 2.), Vector3::new( 0.,  1.,  0.), color),
            make_vertex(pos + Vector3::new( r.x,  r.y, -r.z), Point2::new(2., 3.), Vector3::new( 0.,  1.,  0.), color),
            make_vertex(pos + Vector3::new(-r.x,  r.y, -r.z), Point2::new(1., 3.), Vector3::new( 0.,  1.,  0.), color),
            make_vertex(pos + Vector3::new(-r.x,  r.y,  r.z), Point2::new(1., 2.), Vector3::new( 0.,  1.,  0.), color),

            // y-
            make_vertex(pos + Vector3::new( r.x, -r.y,  r.z), Point2::new(2., 1.), Vector3::new( 0., -1.,  0.), color),
            make_vertex(pos + Vector3::new(-r.x, -r.y,  r.z), Point2::new(1., 1.), Vector3::new( 0., -1.,  0.), color),
            make_vertex(pos + Vector3::new(-r.x, -r.y, -r.z), Point2::new(1., 0.), Vector3::new( 0., -1.,  0.), color),
            make_vertex(pos + Vector3::new( r.x, -r.y, -r.z), Point2::new(2., 0.), Vector3::new( 0., -1.,  0.), color),

            // z+
            make_vertex(pos + Vector3::new( r.x,  r.y,  r.z), Point2::new(2., 2.), Vector3::new( 0.,  0.,  1.), color),
            make_vertex(pos + Vector3::new(-r.x,  r.y,  r.z), Point2::new(1., 2.), Vector3::new( 0.,  0.,  1.), color),
            make_vertex(pos + Vector3::new(-r.x, -r.y,  r.z), Point2::new(1., 1.), Vector3::new( 0.,  0.,  1.), color),
            make_vertex(pos + Vector3::new( r.x, -r.y,  r.z), Point2::new(2., 1.), Vector3::new( 0.,  0.,  1.), color),

            // z-
            make_vertex(pos + Vector3::new( r.x,  r.y, -r.z), Point2::new(2., 3.), Vector3::new( 0.,  0., -1.), color),
            make_vertex(pos + Vector3::new( r.x, -r.y, -r.z), Point2::new(2., 4.), Vector3::new( 0.,  0., -1.), color),
            make_vertex(pos + Vector3::new(-r.x, -r.y, -r.z), Point2::new(1., 4.), Vector3::new( 0.,  0., -1.), color),
            make_vertex(pos + Vector3::new(-r.x,  r.y, -r.z), Point2::new(1., 3.), Vector3::new( 0.,  0., -1.), color),
        ];
        let indices = vec![
             0,  1,  2,  2,  3,  0,
             4,  5,  6,  6,  7,  4,
             8,  9, 10, 10, 11,  8,
            12, 13, 14, 14, 15, 12,
            16, 17, 18, 18, 19, 16,
            20, 21, 22, 22, 23, 20,
        ];
        Model {
            vertices,
            indices,
        }
    }

    pub fn bar(radius: Vector3<f32>, color: Color) -> Model<Vertex> {
        Model::bar_at(Point3::origin(), radius, color)
    }

    pub fn from_heightmap(hm: &HeightMap) -> Model<Vertex> {
        let size = hm.size();
        let heights = hm.heights();
        let mut vertices = Vec::<Vertex>::new();
        let mut indices = Vec::<u32>::new();
        let mut offset = 0;
        let (dimx, dimy) = heights.dim();
        debug_assert_eq!(dimx, dimy);
        let tile_size = size / dimx as f32;
        let htz = tile_size * 0.5;
        let recenter_offset = Vector3::new(dimx as f32 * tile_size * 0.5, 0., dimy as f32 * tile_size * 0.5);
        ndarray::Zip::indexed(heights.windows((2, 2))).apply(|index, view| {
            let center = Point3::new(index.0 as f32 * tile_size, 0., index.1 as f32 * tile_size) - recenter_offset;
            let sw = center + Vector3::new(-htz, view[(0, 0)], -htz);
            let se = center + Vector3::new(-htz, view[(0, 1)],  htz);
            let ne = center + Vector3::new( htz, view[(1, 1)],  htz);
            let nw = center + Vector3::new( htz, view[(1, 0)], -htz);
            let normal = (sw - nw).cross(se - nw).normalize();
            let color = Color::rgb(0, 20, 0);
            vertices.extend(&[
                Vertex::new(sw, Point2::new(0., 0.), normal, color),
                Vertex::new(se, Point2::new(1., 0.), normal, color),
                Vertex::new(ne, Point2::new(1., 1.), normal, color),
                Vertex::new(nw, Point2::new(0., 1.), normal, color),
            ]);
            indices.extend(&[
                offset,
                offset + 1,
                offset + 2,
                offset + 2,
                offset + 3,
                offset,
            ]);
            offset += 4;
        });
        Model {
            vertices,
            indices,
        }
    }
}

pub struct GpuModel<V: glium::Vertex = Vertex> {
    pub vbo: VertexBuffer<V>,
    pub ibo: IndexBuffer<u32>,
}
