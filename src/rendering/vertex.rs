use {
    cgmath::{
        Point2,
        Point3,
        Vector3,
    },
    glium::{
        implement_vertex,
    },
};

use crate::{
    color::Color,
};

#[derive(Debug, Clone, Copy)]
pub struct Vertex {
    pub position: [f32; 3],
    pub uv: [f32; 2],
    pub normal: [f32; 3],
    pub color: [f32; 4],
}

implement_vertex!(Vertex, position, uv, normal, color);

impl Vertex {
    pub fn new(position: Point3<f32>, uv: Point2<f32>, normal: Vector3<f32>, color: Color) -> Vertex {
        Vertex {
            position: position.into(),
            uv: uv.into(),
            normal: normal.into(),
            color: color.into_array(),
        }
    }
}
