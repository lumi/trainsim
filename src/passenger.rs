use specs::prelude::*;

#[derive(Debug, Clone)]
pub struct Passenger {
    pub at: Entity,
    pub dest: Entity,
}
