use {
    cgmath::{prelude::*, Point3, Vector3},
    specs::prelude::*,
};

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct TrackPos {
    pub entity: Entity,
    pub t: f32,
}

impl TrackPos {
    pub fn new(entity: Entity, t: f32) -> TrackPos {
        TrackPos { entity, t }
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Track {
    pub start: Point3<f32>,
    pub end: Point3<f32>,
    pub t_start: f32,
    pub t_end: f32,
}

impl Track {
    pub fn new(start: Point3<f32>, end: Point3<f32>, t_start: f32, t_end: f32) -> Track {
        Track {
            start,
            end,
            t_start,
            t_end,
        }
    }

    pub fn len(&self) -> f32 {
        self.start.distance(self.end)
    }

    pub fn dir(&self) -> Vector3<f32> {
        (self.end - self.start).normalize()
    }

    pub fn point_at(&self, pos: f32) -> Point3<f32> {
        self.start + (self.end - self.start).normalize() * pos
    }

    pub fn tangent_at(&self, _pos: f32) -> Vector3<f32> {
        (self.end - self.start).normalize()
    }
}
