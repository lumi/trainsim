use {
    std::{
        path::Path,
    },
    cgmath::{
        Point2,
        Vector2,
    },
    ndarray::Array2,
    image::{
        ImageResult,
        Luma,
    },
};

#[derive(Debug, Clone)]
pub struct HeightMap {
    size: f32,
    heights: Array2<f32>,
}

impl HeightMap {
    pub fn from_image<P: AsRef<Path>>(path: P, tile_size: f32, base: f32, scale: f32) -> ImageResult<HeightMap> {
        let img = image::open(path)?.into_luma();
        let (w, h) = img.dimensions();
        assert_eq!(w, h); // TODO: shouldn't be an assert
        let size = w as f32 * tile_size;
        let hm = HeightMap {
            size,
            heights: Array2::from_shape_fn((w as usize, w as usize), |(x, y)| {
                let &Luma([height_u32]) = img.get_pixel(x as u32, y as u32);
                let height = base + height_u32 as f32 / 256. * scale;
                height
            })
        };
        Ok(hm)
    }

    pub fn new(size: f32, resolution: usize) -> HeightMap {
        let heights = Array2::zeros((resolution, resolution));
        HeightMap {
            size,
            heights,
        }
    }

    pub fn size(&self) -> f32 {
        self.size
    }

    pub fn heights(&self) -> &Array2<f32> {
        &self.heights
    }

    pub fn height_at(&self, point: Point2<f32>) -> Option<f32> {
        let (size, _) = self.heights.dim();
        let tile_size = self.size / size as f32;
        let real_point = point + Vector2::new(self.size / 2., self.size / 2.);
        if real_point.x < 0. || real_point.y < 0. { return None; }
        if real_point.x >= self.size || real_point.y >= self.size { return None; }
        let cell_pos = (
            (real_point.x / tile_size).floor() as usize,
            (real_point.y / tile_size).floor() as usize,
        );
        let t = (
            (real_point.x / tile_size).floor() - cell_pos.0 as f32,
            (real_point.y / tile_size).floor() - cell_pos.1 as f32,
        );
        let dim = self.heights.dim();
        if cell_pos.0 < 0 || cell_pos.1 < 0 || cell_pos.0 + 1 >= dim.0 || cell_pos.1 + 1 >= dim.1 {
            return None;
        }
        let c00 = self.heights[(cell_pos.0    , cell_pos.1    )];
        let c10 = self.heights[(cell_pos.0 + 1, cell_pos.1    )];
        let c01 = self.heights[(cell_pos.0    , cell_pos.1 + 1)];
        let c11 = self.heights[(cell_pos.0 + 1, cell_pos.1 + 1)];
        let cx0 = c00 * (1. - t.0) + c10 * t.0;
        let cx1 = c01 * (1. - t.0) + c11 * t.0;
        let cxy = cx0 * (1. - t.1) + cx1 * t.1;
        Some(cxy)
    }
}
