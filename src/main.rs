use {
    cgmath::{Point3, Vector3},
    specs::prelude::*,
    std::{thread, time::Duration},
};

#[cfg(feature = "graphics")]
use glium::{
    glutin::{ContextBuilder, Event, EventsLoop, WindowBuilder, WindowEvent},
    Display,
};

mod ai;
mod bezier;
mod camera;
mod color;
mod components;
mod config;
mod input;
mod loaders;
mod passenger;
mod linkgraph;
mod schedule;
mod sim_control;
mod systems;
mod time;
mod track;
mod train_numberer;
mod wagon;
mod heightmap;

use crate::{
    camera::Camera,
    components::{Physical, Rail, Junction, Station, Train, TrainAi, HasStation},
    config::{WINDOW_HEIGHT, WINDOW_WIDTH},
    input::{Cursor, Keyboard},
    linkgraph::{LinkGraph, LinkGraphSystem},
    sim_control::SimControl,
    systems::{
        ClockSystem, CameraSystem, PassengerSpawnerSystem, PassengerSystem, SlotManagerSystem,
        TrainAiSystem, TrainMovementSystem, TrainSpawnerSystem, SimControlSystem,
    },
    time::Clock,
    train_numberer::TrainNumberer,
};

#[cfg(feature = "graphics")]
mod rendering;

#[cfg(feature = "graphics")]
use crate::{input::InputSystem, rendering::Renderer};

#[cfg(feature = "graphics")]
use shrev::EventChannel;

fn make_world() -> World {
    log::info!("creating world");
    let mut world = World::new();
    world.register::<Rail>();
    world.register::<Junction>();
    world.register::<Train>();
    world.register::<TrainAi>();
    world.register::<Station>();
    world.register::<Physical>();
    world.register::<HasStation>();
    let sd = loaders::schedules::load("resources/schedules.txt").unwrap();
    world.insert(sd);
    world.insert(LinkGraph::default());
    world.insert(Camera {
        eye: Point3::new(320., 320., 100.),
        up: Vector3::new(0., 0., 1.),
        ..Default::default()
    });
    world.insert(Cursor::default());
    world.insert(Keyboard::default());
    #[cfg(feature = "graphics")]
    world.insert(EventChannel::<Event>::new());
    world.insert(Clock::default());
    world.insert(heightmap::HeightMap::new(1024., 256));
    world.insert(TrainNumberer::default());
    world.insert(SimControl {
        paused: true,
        pause_next: false,
    });
    loaders::world::load(&mut world, "resources/world.txt").unwrap();
    log::info!("world created");
    world
}

fn make_dispatcher(world: &World) -> Dispatcher<'static, 'static> {
    let mut builder = DispatcherBuilder::new();
    builder.add(ClockSystem, "clock", &[]);
    builder.add_barrier();
    builder.add(LinkGraphSystem, "linkgraph", &[]);
    builder.add(SlotManagerSystem, "slot_manager", &[]);
    builder.add(
        TrainSpawnerSystem,
        "train_spawner",
        &["linkgraph", "slot_manager"],
    );
    builder.add(TrainAiSystem, "train_ai", &["linkgraph", "train_spawner"]);
    builder.add(
        TrainMovementSystem,
        "train_movement",
        &["train_ai", "train_spawner", "linkgraph"],
    );
    builder.add(PassengerSpawnerSystem, "passenger_spawner", &["linkgraph"]);
    builder.add(
        PassengerSystem,
        "passenger",
        &["linkgraph", "passenger_spawner"],
    );
    builder.build()
}

#[cfg(not(feature = "graphics"))]
fn main() {
    pretty_env_logger::init();
    let mut world = make_world();
    let mut dispatcher = make_dispatcher(&mut world);
    loop {
        dispatcher.dispatch(&mut world);
        world.maintain();
        thread::sleep(Duration::from_millis(1000 / 30));
    }
}

#[cfg(feature = "graphics")]
fn main() {
    pretty_env_logger::init();
    let mut events_loop = EventsLoop::new();
    let window = WindowBuilder::new()
        .with_dimensions((WINDOW_WIDTH, WINDOW_HEIGHT).into())
        .with_title("trains");
    let context = ContextBuilder::new()
        .with_vsync(true)
        .with_multisampling(4)
        .with_depth_buffer(24);
    let display = Display::new(window, context, &events_loop).unwrap();

    let mut world = make_world();

    let events_reader = {
        let mut channel = world.write_resource::<EventChannel<Event>>();
        channel.register_reader()
    };

    let mut input_system = InputSystem::new(events_reader);
    let mut sim_control_system = SimControlSystem;
    let mut camera_system = CameraSystem;

    let mut dispatcher = make_dispatcher(&world);

    let mut renderer = Renderer::new(display);

    'main: loop {
        let mut exit = false;
        events_loop.poll_events(|event| {
            match event {
                Event::WindowEvent { ref event, .. } => match event {
                    WindowEvent::CloseRequested => {
                        exit = true;
                    }
                    _ => (),
                },
                _ => (),
            };
            world
                .write_resource::<EventChannel<Event>>()
                .single_write(event);
        });
        if exit {
            break 'main;
        }
        input_system.run_now(&world);
        sim_control_system.run_now(&world);
        if !world.read_resource::<SimControl>().paused {
            dispatcher.dispatch(&world);
        }
        camera_system.run_now(&world);
        world.maintain();
        renderer.run_now(&world);
        thread::sleep(Duration::from_millis(1000 / 60));
    }
}
