use {
    cgmath::{
        prelude::*,
        Matrix4,
        PerspectiveFov,
        Rad,
        Vector2,
        Vector3,
        Point3,
    },
    specs::prelude::*,
};

use crate::config::{WINDOW_HEIGHT, WINDOW_WIDTH};

#[derive(Debug, Clone)]
pub struct Camera {
    pub screen_size: Vector2<f32>,
    pub eye: Point3<f32>,
    pub up: Vector3<f32>,
    pub dir: Vector3<f32>,
    pub state: CameraState,
}

impl Default for Camera {
    fn default() -> Camera {
        Camera {
            screen_size: Vector2::new(WINDOW_WIDTH as f32, WINDOW_HEIGHT as f32),
            eye: Point3::origin(),
            up: Vector3::new(0., 1., 0.),
            dir: Vector3::new(0., 0., 1.),
            state: CameraState::default(),
        }
    }
}

impl Camera {
    pub fn view_matrix(&self) -> Matrix4<f32> {
        Matrix4::look_at_dir(self.eye, self.dir, self.up)
    }

    pub fn perspective_matrix(&self) -> Matrix4<f32> {
        PerspectiveFov {
            fovy: Rad(0.5),
            aspect: self.screen_size.x / self.screen_size.y,
            near: 0.1,
            far: 6000.,
        }
        .into()
    }
}

#[derive(Debug, Clone)]
pub struct CameraState {
    pub focus: Point3<f32>,
    pub offset: Vector3<f32>,
    pub follow: Option<Entity>,
    pub eye_camera: bool,
    pub real_focus: Point3<f32>,
}

impl Default for CameraState {
    fn default() -> CameraState {
        CameraState {
            focus: Point3::origin(),
            offset: Vector3::new(200., 200., 200.),
            follow: None,
            eye_camera: false,
            real_focus: Point3::origin(),
        }
    }
}
