#[macro_export]
macro_rules! pop {
    ($sp:expr, $type:ty) => {
        match $sp.next() {
            Some(item) => match item.parse::<$type>() {
                Ok(ret) => ret,
                Err(err) => {
                    eprintln!("cannot parse item {:?}: {:?}", item, err);
                    continue;
                }
            },
            None => {
                eprintln!("not enough arguments for command");
                continue;
            }
        }
    };
}

pub mod schedules;
pub mod world;
