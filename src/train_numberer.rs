#[derive(Debug, Clone, Default)]
pub struct TrainNumberer {
    pub last_id: usize,
}

impl TrainNumberer {
    pub fn new_id(&mut self) -> usize {
        let id = self.last_id;
        self.last_id += 1;
        id
    }
}
