use {
    specs::prelude::*,
    cgmath::{
        prelude::*,
        Vector3,
        Point3,
    },
};

use crate::{
    bezier::Bezier,
    track::Track,
};

#[derive(Debug, Clone)]
pub struct Rail {
    pub bezier: Bezier,
    pub start_dir: Vector3<f32>,
    pub end_dir: Vector3<f32>,
    pub start_junction: Entity,
    pub end_junction: Entity,
    pub oneway: bool,
    pub signal: bool,
    pub dist: f32,
    pub tracks: Vec<Track>,
    pub reserved: Option<Entity>,
}

impl Rail {
    pub fn new(bezier: Bezier, start_junction: Entity, end_junction: Entity, oneway: bool, signal: bool) -> Rail {
        let mut tracks = Vec::new();
        let mut t = 0.;
        let step = 0.01;
        let mut dist = 0.;
        let mut last_dir = None;
        while t < 1. {
            let start = bezier.point_at(t);
            t += step;
            let end = bezier.point_at(t);
            let last_dist = dist;
            dist += start.distance(end);
            let mut done = false;
            let current_dir = (end - start).normalize();
            if let Some(last_dir) = last_dir {
                if current_dir.dot(last_dir) > 0.999999 {
                    let last: &mut Track = tracks.last_mut().unwrap();
                    last.end = end;
                    last.t_end = dist;
                    done = true;
                }
            }
            if !done {
                tracks.push(Track::new(start, end, last_dist, dist));
            }
            last_dir = Some(current_dir);
        }
        Rail {
            start_dir: bezier.tangent_at(0.).normalize(),
            end_dir: bezier.tangent_at(1.).normalize(),
            bezier,
            start_junction,
            end_junction,
            oneway,
            signal,
            dist,
            tracks,
            reserved: None,
        }
    }

    pub fn point_at(&self, dist: f32) -> Point3<f32> {
        if dist < 0. {
            log::error!("distance on curve less then zero!");
        }
        for t in &self.tracks {
            if t.t_end >= dist {
                return t.point_at(dist - t.t_start);
            }
        }
        log::error!("distance on curve exceeding curve length!");
        let last = self.tracks.last().unwrap();
        last.point_at(dist - last.t_start)
    }

    pub fn tangent_at(&self, dist: f32) -> Vector3<f32> {
        if dist < 0. {
            log::error!("distance on curve less then zero!");
        }
        for t in &self.tracks {
            if t.t_end >= dist {
                return t.dir();
            }
        }
        log::error!("distance on curve exceeding curve length!");
        let last = self.tracks.last().unwrap();
        last.dir()
    }

    pub fn entry_dir(&self) -> Vector3<f32> {
        self.start_dir
    }

    pub fn exit_dir(&self) -> Vector3<f32> {
        self.end_dir
    }
}

impl Component for Rail {
    type Storage = VecStorage<Self>;
}
