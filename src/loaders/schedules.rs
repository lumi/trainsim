use std::{
    collections::HashMap,
    fs::File,
    io::{self, BufRead, BufReader},
    path::Path,
};

use crate::{
    color::Color,
    schedule::{Schedule, ScheduleDirectory, Stop},
    time::Span,
};

pub fn load<P: AsRef<Path>>(path: P) -> io::Result<ScheduleDirectory> {
    let mut sd = ScheduleDirectory {
        schedules: HashMap::new(),
    };
    let mut schedule = None;
    let fd = File::open(path)?;
    for line in BufReader::new(fd).lines() {
        let line = line?;
        let mut sp = line.split(' ');
        let cmd = sp.next().unwrap(); // First one always succeeds.
        match cmd {
            "SCHEDULE" => {
                let name = pop!(sp, String);
                schedule = Some(Schedule {
                    name,
                    stops: Vec::new(),
                    every: Span(0, 30),
                    slots: Vec::new(),
                    wagons: 3,
                    color: Color::new(0., 0., 0., 0.),
                });
            }
            "WAGONS" => {
                let count = pop!(sp, usize);
                if let Some(ref mut schedule) = schedule {
                    schedule.wagons = count;
                } else {
                    log::error!("invalid state for this command");
                }
            }
            "EVERY" => {
                let delta = pop!(sp, Span);
                if let Some(ref mut schedule) = schedule {
                    schedule.every = delta;
                } else {
                    log::error!("invalid state for this command");
                }
            }
            "STOP" => {
                let name = pop!(sp, String);
                let offset = pop!(sp, Span);
                if let Some(ref mut schedule) = schedule {
                    schedule.stops.push(Stop { name, offset });
                } else {
                    log::error!("invalid state for this command");
                }
            }
            "COLOR" => {
                let r = pop!(sp, u8);
                let g = pop!(sp, u8);
                let b = pop!(sp, u8);
                if let Some(ref mut schedule) = schedule {
                    schedule.color = Color::rgb(r, g, b);
                } else {
                    log::error!("invalid state for this command");
                }
            }
            "DONE" => {
                if let Some(schedule) = schedule.take() {
                    sd.schedules.insert(schedule.name.to_owned(), schedule);
                } else {
                    log::error!("invalid state for this command");
                }
            }
            _ => (),
        }
    }
    Ok(sd)
}
