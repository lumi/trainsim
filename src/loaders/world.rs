use {
    cgmath::{
        Point2,
        Point3,
        Vector3,
    },
    specs::prelude::*,
    std::{
        collections::HashMap,
        fs::File,
        io::{self, BufRead, BufReader},
        path::Path,
    },
};

use crate::{
    bezier::Bezier,
    color::Color,
    components::{Rail, Station, HasStation, Junction, JunctionExit},
    track::TrackPos,
    heightmap::HeightMap,
};

struct JunctionData {
    entity: Entity,
    pos: Point3<f32>,
}

struct TrackState {
    last_jent: Entity,
    last_pos: Point3<f32>,
    last_dir: Vector3<f32>,
}

pub fn load<P: AsRef<Path>>(
    world: &mut World,
    path_ref: P,
) -> io::Result<()> {
    let path = path_ref.as_ref();
    let dir = path.parent().unwrap().to_owned();
    let fd = File::open(path)?;
    let mut track_state = None;
    let mut stations = HashMap::new();
    let mut junctions = HashMap::new();
    for line in BufReader::new(fd).lines() {
        let line = line?;
        let mut sp = line.split(' ');
        let cmd = sp.next().unwrap(); // First one always succeeds.
        match cmd {
            "HEIGHTMAP" => {
                let rel_path = pop!(sp, String);
                let tile_size: f32 = pop!(sp, f32);
                let base: f32 = pop!(sp, f32);
                let scale: f32 = pop!(sp, f32);
                let hm_path = dir.join(rel_path);
                let hm = HeightMap::from_image(hm_path, tile_size, base, scale).unwrap();
                world.insert(hm);
            },
            "STATION" => {
                let station_name = pop!(sp, String);
                let r = pop!(sp, u8);
                let g = pop!(sp, u8);
                let b = pop!(sp, u8);
                let ent = world.create_entity().with(Station {
                    name: station_name.clone(),
                    color: Color::rgb(r, g, b),
                    passengers: Vec::new(),
                    rails: Vec::new(),
                }).build();
                stations.insert(station_name, ent);
            },
            "JUNCTION" => {
                let name = pop!(sp, String);
                let x = pop!(sp, f32);
                let y = pop!(sp, f32);
                let z = pop!(sp, f32);
                let pos = Point3::new(x, y, z);
                let entity = world.create_entity()
                     .with(Junction {
                         name: name.clone(),
                         pos,
                         exits: Vec::new(),
                     })
                     .build();
                junctions.insert(name, JunctionData {
                    entity,
                    pos,
                });
            },
            "START" => {
                let jn = pop!(sp, String);
                let dx = pop!(sp, f32);
                let dy = pop!(sp, f32);
                let dz = pop!(sp, f32);
                let jd = &junctions[&jn];
                let dir = Vector3::new(dx, dy, dz);
                track_state = Some(TrackState {
                    last_jent: jd.entity,
                    last_pos: jd.pos,
                    last_dir: dir,
                });
            },
            "NEXT" => {
                let jn = pop!(sp, String);
                let dx = pop!(sp, f32);
                let dy = pop!(sp, f32);
                let dz = pop!(sp, f32);
                let mut station_entity = None;
                let mut oneway = false;
                let mut signal = false;
                let mut stick = true;
                let mut platform_left = false;
                loop {
                    if let Some(prop) = sp.next() {
                        match prop {
                            "STATION" => {
                                let station_name = pop!(sp, String);
                                station_entity = Some(stations.get(&station_name).unwrap().clone());
                            },
                            "ONEWAY" => {
                                oneway = true;
                            },
                            "SIGNAL" => {
                                signal = true;
                            },
                            "NOSTICK" => {
                                stick = false;
                            },
                            "PLATFORM-LEFT" => {
                                platform_left = true;
                            },
                            _ => (),
                        }
                    }
                    else {
                        break;
                    }
                }
                let hm = world.fetch::<HeightMap>();
                let jd = &junctions[&jn];
                let dir = Vector3::new(dx, dy, dz);
                let track_state = track_state.as_mut().unwrap();
                let mut start = track_state.last_pos;
                let mut end = jd.pos;
                if stick {
                    let up_start = Vector3::new(0., hm.height_at(Point2::new(start.x, start.z)).unwrap_or(0.) + 3., 0.);
                    let up_end = Vector3::new(0., hm.height_at(Point2::new(end.x, end.z)).unwrap_or(0.) + 3., 0.);
                    start += up_start;
                    end += up_end;
                }
                let control_1 = start + track_state.last_dir;
                let control_2 = end - dir;
                let bezier = Bezier::new(start, control_1, control_2, end);
                drop(hm);
                let rail = Rail::new(bezier, track_state.last_jent, jd.entity, oneway, signal);
                let dist = rail.dist;
                let entry_dir = rail.entry_dir();
                let exit_dir = rail.exit_dir();
                let mut builder = world
                    .create_entity()
                    .with(rail);
                if let Some(ent) = station_entity {
                    builder = builder.with(HasStation::new(ent, platform_left));
                }
                let me = builder.build();
                let mut js = world.write_component::<Junction>();
                {
                    let j = js.get_mut(track_state.last_jent).unwrap();
                    j.exits.push(JunctionExit {
                        next: TrackPos::new(me, 0.),
                        dist,
                        entry_dir,
                        exit_dir,
                        signal,
                    });
                }
                if !oneway {
                    let j = js.get_mut(jd.entity).unwrap();
                    j.exits.push(JunctionExit {
                        next: TrackPos::new(me, dist),
                        dist,
                        entry_dir: -exit_dir,
                        exit_dir: -entry_dir,
                        signal,
                    });
                }
                track_state.last_jent = jd.entity;
                track_state.last_pos = jd.pos;
                track_state.last_dir = dir;
            },
            _ => (),
        }
    }
    Ok(())
}
