const INVERTED_255: f32 = 1. / 255.;

#[derive(Debug, Clone, Copy)]
pub struct Color {
    pub red: f32,
    pub green: f32,
    pub blue: f32,
    pub alpha: f32,
}

impl Color {
    pub fn new(red: f32, green: f32, blue: f32, alpha: f32) -> Color {
        Color {
            red,
            green,
            blue,
            alpha,
        }
    }

    pub fn rgb(red: u8, green: u8, blue: u8) -> Color {
        Color {
            red: red as f32 * INVERTED_255,
            green: green as f32 * INVERTED_255,
            blue: blue as f32 * INVERTED_255,
            alpha: 1.,
        }
    }

    pub fn into_array(self) -> [f32; 4] {
        [self.red, self.green, self.blue, self.alpha]
    }

    pub fn normalize(self) -> Color {
        let max = if self.red > self.green {
            if self.blue > self.red {
                self.blue
            } else {
                self.red
            }
        } else if self.blue > self.green {
            self.blue
        } else {
            self.green
        };
        let imax = 1. / max;
        Color {
            red: self.red * imax,
            green: self.green * imax,
            blue: self.blue * imax,
            alpha: self.alpha,
        }
    }

    pub fn lighter(self) -> Color {
        Color {
            red: self.red + 0.1,
            green: self.green + 0.1,
            blue: self.blue + 0.1,
            alpha: self.alpha,
        }
        .normalize()
    }
}

impl From<[f32; 3]> for Color {
    fn from(arr: [f32; 3]) -> Color {
        Color {
            red: arr[0],
            green: arr[1],
            blue: arr[2],
            alpha: 1.,
        }
    }
}
