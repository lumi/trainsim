use {
    cgmath::{
        prelude::*,
        Matrix4,
        Vector3,
        Vector4,
    },
    glium::{
        self,
        uniform,
        Display,
        Program,
        Surface,
        BackfaceCullingMode,
        texture::{
            RawImage2d,
            Texture2d,
        },
    },
    glium_text::{self, FontTexture, TextDisplay, TextSystem},
    specs::prelude::*,
    image::{
        GenericImageView,
    },
    std::{
        fs::{
            self,
            File,
        },
        path::{
            Path,
            PathBuf,
        },
    },
};

use crate::{
    camera::{
        Camera,
    },
    color::Color,
    components::{Physical, Rail, Station, Train, TrainAi, HasStation},
    config::{WINDOW_HEIGHT, WINDOW_WIDTH},
    schedule::ScheduleDirectory,
    time::Clock,
    heightmap::HeightMap,
};

mod vertex;
mod model;

use model::{Model, GpuModel};

pub struct Renderer {
    display: Display,
    terrain_program: Program,
    program: Program,
    heightmap_gpu_model: Option<GpuModel>,
    train_gpu_model: Option<GpuModel>,
    station_gpu_model: Option<GpuModel>,
    station_floor_gpu_model: Option<GpuModel>,
    rail_gpu_model: Option<GpuModel>,
    text_system: TextSystem,
    font_texture: FontTexture,
    transparent_texture: Texture2d,
    train_texture: Texture2d,
}

fn load_texture<F: glium::backend::Facade>(facade: &F, path: impl AsRef<Path>) -> Texture2d {
    let path = path.as_ref();
    log::info!("loading texture {}", path.display());
    let root: PathBuf = ["resources"].iter().collect();
    let full_path = root.join(path);
    let img = image::open(full_path).unwrap();
    let dim = img.dimensions();
    let raw = RawImage2d::from_raw_rgba_reversed(&img.into_rgba(), dim);
    Texture2d::new(facade, raw).unwrap()
}

fn load_shader_program<F: glium::backend::Facade>(facade: &F, name: &str) -> Program {
    log::info!("loading shaders for {}", name);
    let path: PathBuf = ["resources", "shaders"].iter().collect();
    let vertex_shader_path = path.join(format!("{}.vertex.glsl", name));
    let fragment_shader_path = path.join(format!("{}.fragment.glsl", name));
    let vertex_shader_data = fs::read_to_string(vertex_shader_path).expect("couldn't load vertex shader");
    let fragment_shader_data = fs::read_to_string(fragment_shader_path).expect("couldn't load fragment shader");
    Program::from_source(facade, &vertex_shader_data, &fragment_shader_data, None).expect("couldn't load shader program")
}

impl Renderer {
    pub fn new(display: Display) -> Renderer {
        let program = load_shader_program(&display, "entity");
        let terrain_program = load_shader_program(&display, "terrain");
        let text_system = TextSystem::new(&display);
        let font_texture = FontTexture::new(
            &display,
            File::open("resources/NotoSans-Black.ttf").unwrap(),
            64,
        )
        .unwrap();
        let transparent_texture_raw = RawImage2d::from_raw_rgba(vec![0u8, 0, 0, 0], (1, 1));
        let transparent_texture = Texture2d::new(&display, transparent_texture_raw).unwrap();
        let train_texture = load_texture(&display, "train.png");
        Renderer {
            display,
            program,
            terrain_program,
            heightmap_gpu_model: None,
            train_gpu_model: None,
            station_gpu_model: None,
            station_floor_gpu_model: None,
            rail_gpu_model: None,
            text_system,
            font_texture,
            transparent_texture,
            train_texture,
        }
    }
}

impl<'a> System<'a> for Renderer {
    type SystemData = (
        Entities<'a>,
        Read<'a, Clock>,
        Read<'a, ScheduleDirectory>,
        Read<'a, Camera>,
        ReadExpect<'a, HeightMap>,
        ReadStorage<'a, Physical>,
        ReadStorage<'a, Train>,
        ReadStorage<'a, TrainAi>,
        ReadStorage<'a, Rail>,
        ReadStorage<'a, HasStation>,
        ReadStorage<'a, Station>,
    );

    fn run(
        &mut self,
        (
            _entities,
            clock,
            sd,
            cam,
            hm,
            phys,
            trains,
            ais,
            rails,
            hss,
            stations,
        ): Self::SystemData,
    ) {
        if self.heightmap_gpu_model.is_none() {
            let model = Model::from_heightmap(&*hm);
            self.heightmap_gpu_model = Some(model.to_gpu_model(&self.display));
        }
        if self.train_gpu_model.is_none() {
            let model = Model::load_from_obj("resources/train.obj", "Train").unwrap();
            self.train_gpu_model = Some(model.to_gpu_model(&self.display));
        }
        if self.station_gpu_model.is_none() {
            let model = Model::load_from_obj("resources/station.obj", "Station").unwrap();
            self.station_gpu_model = Some(model.to_gpu_model(&self.display));
        }
        if self.station_floor_gpu_model.is_none() {
            let model = Model::bar(
                Vector3::new(8., 1., 0.5),
                Color::new(0.7, 0.7, 0.7, 0.8),
            );
            self.station_floor_gpu_model = Some(model.to_gpu_model(&self.display));
        }
        if self.rail_gpu_model.is_none() {
            let model = Model::bar(
                Vector3::new(0.4, 0.4, 0.5),
                Color::new(0., 0., 0., 0.),
            );
            self.rail_gpu_model = Some(model.to_gpu_model(&self.display));
        }
        if cam.up.y <= 0. {
            log::warn!("CAMERA INVERTED");
        }
        let mut target = self.display.draw();
        target.clear_color_and_depth((0.6, 1., 1., 1.), 1.);
        let view: [[f32; 4]; 4] = cam.view_matrix().into();
        let perspective: [[f32; 4]; 4] = cam.perspective_matrix().into();
        let params = glium::DrawParameters {
            depth: glium::Depth {
                test: glium::draw_parameters::DepthTest::IfLess,
                write: true,
                ..Default::default()
            },
            backface_culling: BackfaceCullingMode::CullClockwise,
            ..Default::default()
        };
        let hm_model: [[f32; 4]; 4] = Matrix4::identity().into();
        let hm_gpu_model = self.heightmap_gpu_model.as_ref().unwrap();
        target
            .draw(
                &hm_gpu_model.vbo,
                &hm_gpu_model.ibo,
                &self.terrain_program,
                &uniform! { view: view, perspective: perspective, model: hm_model, side_color: [0.6f32, 0.6, 0.6, 1.] },
                &params,
            )
            .unwrap();
        let tm = self.train_gpu_model.as_ref().unwrap();
        for (train, phy, ai) in (&trains, &phys, &ais).join() {
            let mut coff = 0.;
            let mut nidx = 0;
            let mut cpos = phy.pos;
            'wagons: for wagon in &train.wagons {
                coff += wagon.offset;
                loop {
                    if let Some(&npos) = train.recent.get(nidx) {
                        let delta = npos - cpos;
                        let len = delta.magnitude();
                        if coff >= len {
                            coff -= len;
                            cpos = npos;
                            nidx += 1;
                        } else {
                            break;
                        }
                    } else {
                        // TODO: expand train track cache so this never happens
                        break 'wagons;
                    }
                }
                let npos = train.recent[nidx];
                let delta = npos - cpos;
                let dir = delta.normalize();
                let perp = Vector3::new(dir.z, 0., -dir.x).normalize();
                let up = dir.cross(perp).normalize();
                let rot = Matrix4::from_cols(
                    perp.extend(0.),
                    up.extend(0.),
                    dir.extend(0.),
                    Vector4::new(0., 0., 0., 1.),
                );
                let world_pos = cpos + coff * dir;
                let modmat = Matrix4::from_translation(world_pos.to_vec()) * rot;
                let model_matrix: [[f32; 4]; 4] = modmat.into();
                let train_color = if let Some(ref schedule) = ai.schedule {
                    let schedule = &sd.schedules[schedule];
                    schedule.color
                } else {
                    Color::rgb(255, 255, 255)
                };
                target.draw(
                    &tm.vbo,
                    &tm.ibo,
                    &self.program,
                    &uniform! { view: view, perspective: perspective, model: model_matrix, default_color: train_color.into_array(), tex: &self.train_texture },
                    &params,
                ).unwrap();
            }
        }
        let sm = self.station_gpu_model.as_ref().unwrap();
        for (rail, hs) in (&rails, &hss).join() {
            let station = stations.get(hs.station).unwrap();
            for track in &rail.tracks {
                let track_len = track.len();
                let tw_middle = track.point_at(track_len * 0.5).to_vec();
                let along = track.tangent_at(track_len * 0.5);
                let perp = Vector3::new(along.z, 0., -along.x).normalize();
                let up = along.cross(perp).normalize();
                let rot = Matrix4::from_cols(
                    perp.extend(0.),
                    up.extend(0.),
                    along.extend(0.),
                    Vector4::new(0., 0., 0., 1.),
                );
                let station_offset = if hs.platform_left {
                    Vector3::new(7.2, 0., 0.)
                }
                else {
                    Vector3::new(-7.2, 0., 0.)
                };
                let scale = Matrix4::from_nonuniform_scale(1., 1., track_len);
                let modmat = Matrix4::from_translation(tw_middle) * rot * Matrix4::from_translation(station_offset) * scale;
                let model_matrix: [[f32; 4]; 4] = modmat.into();
                target.draw(
                    &sm.vbo,
                    &sm.ibo,
                    &self.program,
                    &uniform! { view: view, perspective: perspective, model: model_matrix, default_color: station.color.into_array(), tex: &self.transparent_texture },
                    &params,
                ).unwrap();
            }
        }
        let sfm = self.station_floor_gpu_model.as_ref().unwrap();
        for (rail, hs) in (&rails, &hss).join() {
            let station = stations.get(hs.station).unwrap();
            for track in &rail.tracks {
                let track_len = track.len();
                let tw_middle = track.point_at(track_len * 0.5).to_vec();
                let along = track.tangent_at(track_len * 0.5);
                let perp = Vector3::new(along.z, 0., -along.x).normalize();
                let up = along.cross(perp).normalize();
                let rot = Matrix4::from_cols(
                    perp.extend(0.),
                    up.extend(0.),
                    along.extend(0.),
                    Vector4::new(0., 0., 0., 1.),
                );
                let scale = Matrix4::from_nonuniform_scale(1., 1., track_len);
                let modmat = Matrix4::from_translation(tw_middle - Vector3::new(0., 2., 0.)) * rot * scale;
                let model_matrix: [[f32; 4]; 4] = modmat.into();
                target.draw(
                    &sfm.vbo,
                    &sfm.ibo,
                    &self.program,
                    &uniform! { view: view, perspective: perspective, model: model_matrix, default_color: station.color.into_array(), tex: &self.transparent_texture },
                    &params,
                ).unwrap();
            }
        }
        let rm = self.rail_gpu_model.as_ref().unwrap();
        for rail in (&rails).join() {
            for track in &rail.tracks {
                let color = if let Some(ent) = rail.reserved {
                    if let Some(ai) = ais.get(ent) {
                        if let Some(ref schedule_name) = ai.schedule {
                            if let Some(schedule) = sd.schedules.get(schedule_name) {
                                schedule.color.lighter()
                            }
                            else {
                                Color::new(1., 1., 1., 1.)
                            }
                        }
                        else {
                            Color::new(1., 0., 0., 1.)
                        }
                    }
                    else {
                        Color::new(1., 0., 0., 1.)
                    }
                }
                else {
                    Color::new(0.6, 0.6, 0.6, 1.)
                };
                let len = track.len();
                let mid = track.point_at(len * 0.5);
                let along = track.tangent_at(len * 0.5);
                let perp = Vector3::new(along.z, 0., -along.x).normalize();
                let up = along.cross(perp).normalize();
                let rot = Matrix4::from_cols(
                    perp.extend(0.),
                    up.extend(0.),
                    along.extend(0.),
                    Vector4::new(0., 0., 0., 1.),
                );
                let scale = Matrix4::from_nonuniform_scale(1., 1., track.len());
                let modmat = Matrix4::from_translation(mid.to_vec()) * rot * scale;
                let model_matrix: [[f32; 4]; 4] = modmat.into();
                target.draw(
                    &rm.vbo,
                    &rm.ibo,
                    &self.program,
                    &uniform! { view: view, perspective: perspective, model: model_matrix, default_color: color.into_array(), tex: &self.transparent_texture },
                    &params,
                ).unwrap();
                // TODO: one way
                // TODO: signal
            }
        }
        let mut status_text = format!("[{}]", clock.now);
        if let Some(entity) = cam.state.follow {
            if let (Some(phy), Some(ai)) = (phys.get(entity), ais.get(entity)) {
                status_text += &format!("{:3.0} {:?}:", phy.vel.magnitude(), ai.state);
                if let Some(ref schedule_name) = ai.schedule {
                    if let Some(slot_id) = ai.slot {
                        let schedule = &sd.schedules[schedule_name];
                        let slot = &schedule.slots[slot_id];
                        for stop in &schedule.stops[ai.schedule_idx..] {
                            status_text += &format!(" {}@{}", stop.name, slot.start + stop.offset);
                        }
                    } else {
                        status_text += " No slot";
                    }
                } else {
                    status_text += "No schedule";
                }
            }
        }
        let td = TextDisplay::new(&self.text_system, &self.font_texture, &status_text);
        let matrix = [
            [32. / WINDOW_WIDTH as f32, 0., 0., 0.],
            [0., 32. / WINDOW_HEIGHT as f32, 0., 0.],
            [0., 0., 1., 0.],
            [-1., -1., 0., 1.],
        ];
        glium_text::draw(
            &td,
            &self.text_system,
            &mut target,
            matrix,
            (1., 1., 0., 1.),
        );
        target.finish().unwrap();
    }
}
