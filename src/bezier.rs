use {
    cgmath::{
        prelude::*,
        Point3,
        Vector3,
    },
};

#[derive(Debug, Clone)]
pub struct Bezier {
    pub start: Point3<f32>,
    pub control_start: Point3<f32>,
    pub control_end: Point3<f32>,
    pub end: Point3<f32>,
    pub a: Vector3<f32>,
    pub b: Vector3<f32>,
    pub c: Vector3<f32>,
}

impl Bezier {
    pub fn new(start: Point3<f32>, control_start: Point3<f32>, control_end: Point3<f32>, end: Point3<f32>) -> Bezier {
        let a = -      start.to_vec()
                + 3. * control_start.to_vec()
                - 3. * control_end.to_vec()
                +      end.to_vec();
        let b =   3. * start.to_vec()
                - 6. * control_start.to_vec()
                + 3. * control_end.to_vec();
        let c = - 3. * start.to_vec()
                + 3. * control_start.to_vec();
        Bezier {
            start,
            control_start,
            control_end,
            end,
            a,
            b,
            c,
        }
    }

    pub fn point_at(&self, t: f32) -> Point3<f32> {
        let t2 = t * t;
        let t3 = t2 * t;
        Point3::from_vec(
              t3 * self.a
            + t2 * self.b
            + t  * self.c
            +      self.start.to_vec())
    }

    pub fn tangent_at(&self, t: f32) -> Vector3<f32> {
        let t2 = t * t;
        let tan = 3. * t2 * self.a
                + 2. * t  * self.b
                +           self.c;
        tan
    }

    pub fn split(&self) -> (Bezier, Bezier) {
        let b_0 = self.start;
        let b_1 = Point3::from_vec((self.start.to_vec() + self.control_start.to_vec()) * 0.5);
        let b_2 = Point3::from_vec((self.start.to_vec() + 2. * self.control_start.to_vec() + self.control_end.to_vec()) * 0.25);
        let b_3 = Point3::from_vec((self.start.to_vec() + 3. * self.control_start.to_vec() + 3. * self.control_end.to_vec() + self.end.to_vec()) * 0.125);
        let b_4 = Point3::from_vec((self.control_start.to_vec() + 2. * self.control_end.to_vec() + self.end.to_vec()) * 0.25);
        let b_5 = Point3::from_vec((self.control_end.to_vec() + self.end.to_vec()) * 0.5);
        let b_6 = self.end;
        let bezier_start = Bezier::new(
            b_0,
            b_1,
            b_2,
            b_3,
        );
        let bezier_end = Bezier::new(
            b_3,
            b_4,
            b_5,
            b_6,
        );
        (bezier_start, bezier_end)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use approx::assert_ulps_eq;

    #[test]
    fn test_bezier_tangent() {
        let bezier = Bezier::new(
            Point3::new(-100., 0., 0.),
            Point3::new(-100., 0., 100.),
            Point3::new(100., 0., 100.,),
            Point3::new(100., 0., 0.),
        );
        let mut t = 0.;
        while t <= 1. {
            let p1 = bezier.point_at(t);
            let p2 = bezier.point_at(t + 0.0001);
            let delta = p2 - p1;
            let tan = bezier.tangent_at(t);
            let dot = tan.normalize().dot(delta.normalize());
            assert_ulps_eq!(dot, 1., epsilon = 0.0001);
            t += 0.01;
        }
    }
}
