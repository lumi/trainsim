use std::{
    cmp::Ordering,
    fmt,
    num::ParseIntError,
    ops::{Add, AddAssign, Sub, SubAssign},
    str::FromStr,
    time::Instant,
};

#[derive(Debug, Clone)]
pub struct Clock {
    pub now: Time,
    start_time: Time,
    start_instant: Instant,
}

impl Default for Clock {
    fn default() -> Clock {
        Clock {
            now: Time(12, 0),
            start_time: Time(12, 0),
            start_instant: Instant::now(),
        }
    }
}

impl Clock {
    pub fn update(&mut self) {
        let ms = self.start_instant.elapsed().as_millis();
        let total_minutes = ms / 500;
        let (hours, minutes) = (total_minutes as i32 / 60, total_minutes as i32 % 60);
        self.now = self.start_time + Span(hours, minutes);
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Time(pub i32, pub i32);

impl fmt::Display for Time {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:0>2}:{:0>2}", self.0, self.1)
    }
}

impl PartialOrd<Time> for Time {
    fn partial_cmp(&self, other: &Time) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Time {
    fn cmp(&self, other: &Time) -> Ordering {
        let me = self.normalize();
        let other = other.normalize();
        me.0.cmp(&other.0).then(me.1.cmp(&other.1))
    }
}

impl PartialEq<Time> for Time {
    fn eq(&self, other: &Time) -> bool {
        self.0 == other.0 && self.1 == other.1
    }
}

impl Eq for Time {}

impl Add<Span> for Time {
    type Output = Time;

    fn add(self, other: Span) -> Time {
        Time(self.0 + other.0, self.1 + other.1).normalize()
    }
}

impl AddAssign<Span> for Time {
    fn add_assign(&mut self, other: Span) {
        *self = *self + other;
    }
}

impl Sub<Span> for Time {
    type Output = Time;

    fn sub(self, other: Span) -> Time {
        Time(self.0 - other.0, self.1 - other.1).normalize()
    }
}

impl SubAssign<Span> for Time {
    fn sub_assign(&mut self, other: Span) {
        *self = *self - other;
    }
}

impl Time {
    pub fn normalize(self) -> Time {
        let (div, rem) = (self.1 / 60, self.1 % 60);
        let hours = (self.0 + div) % 24;
        Time(hours, rem)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct Span(pub i32, pub i32);

#[derive(Debug)]
pub enum ParseTimeError {
    ParseIntError(ParseIntError),
    NoColon,
    TooManyColons,
}

impl From<ParseIntError> for ParseTimeError {
    fn from(err: ParseIntError) -> ParseTimeError {
        ParseTimeError::ParseIntError(err)
    }
}

impl FromStr for Span {
    type Err = ParseTimeError;

    fn from_str(s: &str) -> Result<Span, Self::Err> {
        let mut sp = s.split(':');
        let hours_str = sp.next().unwrap();
        let hours = hours_str.parse()?;
        if let Some(minutes_str) = sp.next() {
            let minutes = minutes_str.parse()?;
            if sp.next().is_some() {
                Err(ParseTimeError::TooManyColons)
            } else {
                Ok(Span(hours, minutes))
            }
        } else {
            Err(ParseTimeError::NoColon)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn time_ordering_works() {
        assert!(Time(1, 0) == Time(1, 0));
        assert!(Time(2, 0) > Time(1, 0));
        assert!(Time(1, 0) < Time(2, 0));
        assert!(Time(1, 30) > Time(1, 0));
        assert!(Time(10, 0) > Time(1, 30));
    }
}
