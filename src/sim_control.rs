#[derive(Debug, Clone, Default)]
pub struct SimControl {
    pub paused: bool,
    pub pause_next: bool,
}
