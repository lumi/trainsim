use {
    cgmath::{Point3, Vector3},
    specs::prelude::*,
    std::collections::VecDeque,
};

use crate::{
    ai::TrainAiState,
    color::Color,
    passenger::Passenger,
    wagon::Wagon,
    track::TrackPos,
};

mod rail;

pub use rail::Rail;

#[derive(Debug, Clone)]
pub struct Physical {
    pub pos: Point3<f32>,
    pub vel: Vector3<f32>,
}

impl Physical {
    pub fn new(pos: Point3<f32>, vel: Vector3<f32>) -> Physical {
        Physical { pos, vel }
    }
}

impl Component for Physical {
    type Storage = VecStorage<Self>;
}

#[derive(Debug, Clone)]
pub struct Train {
    pub id: usize,
    pub pos: TrackPos,
    pub throttle: f32,
    pub brakes: bool,
    pub dir: Vector3<f32>,
    pub plan: VecDeque<Entity>,
    pub wagons: Vec<Wagon>,
    pub closed: bool,
    pub recent: VecDeque<Point3<f32>>,
}

impl Train {
    pub fn new(id: usize, pos: TrackPos, dir: Vector3<f32>, wagons: usize, capacity: usize) -> Train {
        Train {
            id,
            pos,
            throttle: 0.,
            brakes: false,
            dir,
            plan: VecDeque::new(),
            closed: false,
            wagons: (0..wagons)
                .map(|i| Wagon::new(if i == 0 { 0. } else { 20.5 }, capacity))
                .collect(),
            recent: VecDeque::new(),
        }
    }
}

impl Component for Train {
    type Storage = VecStorage<Self>;
}

#[derive(Debug, Clone)]
pub struct TrainAi {
    pub schedule: Option<String>,
    pub schedule_idx: usize,
    pub state: TrainAiState,
    pub slot: Option<usize>,
}

impl TrainAi {
    pub fn new() -> TrainAi {
        TrainAi {
            schedule: None,
            schedule_idx: 0,
            state: TrainAiState::Idle,
            slot: None,
        }
    }
}

impl Component for TrainAi {
    type Storage = VecStorage<Self>;
}

#[derive(Debug, Clone)]
pub struct Station {
    pub name: String,
    pub color: Color,
    pub rails: Vec<Entity>,
    pub passengers: Vec<Passenger>,
}

impl Component for Station {
    type Storage = VecStorage<Self>;
}

#[derive(Debug, Clone)]
pub struct HasStation {
    pub station: Entity,
    pub platform_left: bool,
}

impl HasStation {
    pub fn new(station: Entity, platform_left: bool) -> HasStation {
        HasStation {
            station,
            platform_left,
        }
    }
}

impl Component for HasStation {
    type Storage = VecStorage<Self>;
}

#[derive(Debug, Clone)]
pub struct Junction {
    pub name: String,
    pub pos: Point3<f32>,
    pub exits: Vec<JunctionExit>,
}

impl Component for Junction {
    type Storage = VecStorage<Self>;
}

#[derive(Debug, Clone)]
pub struct JunctionExit {
    pub next: TrackPos,
    pub dist: f32,
    pub entry_dir: Vector3<f32>,
    pub exit_dir: Vector3<f32>,
    pub signal: bool,
}
