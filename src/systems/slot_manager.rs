use specs::prelude::*;

use crate::{
    ai::TrainAiState,
    components::{Station, Train, TrainAi, HasStation},
    schedule::{ScheduleDirectory, Slot},
    time::{Clock, Time},
};

pub struct SlotManagerSystem;

impl<'a> System<'a> for SlotManagerSystem {
    type SystemData = (
        Entities<'a>,
        Read<'a, Clock>,
        Write<'a, ScheduleDirectory>,
        ReadStorage<'a, Train>,
        WriteStorage<'a, TrainAi>,
        ReadStorage<'a, HasStation>,
        ReadStorage<'a, Station>,
    );

    fn run(&mut self, (ents, clock, mut sd, trains, mut ais, hss, stations): Self::SystemData) {
        for schedule in sd.schedules.values_mut() {
            if schedule.slots.iter().all(|s| s.train.is_some()) {
                schedule.slots.clear();
            }
            if schedule.slots.is_empty() {
                let mut start = Time(12, 0);
                let delta = schedule.every;
                while start < Time(23, 0) && start >= Time(12, 0) {
                    let id = schedule.slots.len();
                    log::info!("created new slot {} (at {}) for schedule {}", id, start, schedule.name);
                    schedule.slots.push(Slot {
                        id,
                        start,
                        train: None,
                    });
                    start += delta;
                }
            }
        }
        for (ent, train, ai) in (&ents, &trains, &mut ais).join() {
            if let Some(hs) = hss.get(train.pos.entity) {
                let station = stations.get(hs.station).unwrap();
                if ai.slot.is_none() && ai.state == TrainAiState::Idle {
                    let mut slot_data = None;
                    let mut slot_time = Time(23, 59);
                    for schedule in sd.schedules.values() {
                        if station.name != schedule.stops[0].name {
                            continue;
                        }
                        for slot in &schedule.slots {
                            if slot.start > slot_time {
                                continue;
                            }
                            if slot.start < clock.now {
                                continue;
                            }
                            if slot.train.is_some() {
                                continue;
                            }
                            slot_data = Some((schedule.name.to_owned(), slot.id));
                            slot_time = slot.start;
                        }
                    }
                    if let Some((schedule_name, slot_id)) = slot_data {
                        let schedule = sd.schedules.get_mut(&schedule_name).unwrap();
                        let slot = &mut schedule.slots[slot_id];
                        log::info!("assigning slot {} (at {}) for schedule {} to train {}", slot.id, slot.start, schedule_name, train.id);
                        slot.train = Some(ent);
                        ai.schedule = Some(schedule_name);
                        ai.slot = Some(slot.id);
                        ai.schedule_idx = 0;
                    }
                    else {
                        log::info!("can't find a slot for train {}", train.id);
                    }
                }
            }
        }
    }
}
