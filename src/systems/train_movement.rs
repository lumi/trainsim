use {
    cgmath::{prelude::*, Vector3},
    specs::prelude::*,
};

use crate::{
    components::{Physical, Rail, Junction, Train},
    config::MIN_TURN_DOT,
    track::TrackPos,
};

pub struct TrainMovementSystem;

impl<'a> System<'a> for TrainMovementSystem {
    type SystemData = (
        WriteStorage<'a, Rail>,
        ReadStorage<'a, Junction>,
        WriteStorage<'a, Physical>,
        WriteStorage<'a, Train>,
    );

    fn run(&mut self, (mut rails, junctions, mut phys, mut trains): Self::SystemData) {
        for (mut train, mut phy) in (&mut trains, &mut phys).join() {
            phy.vel *= if train.brakes { 0.9 } else { 0.99 };
            phy.vel += train.dir * (train.throttle * 0.1);
            if let Some(rail) = rails.get_mut(train.pos.entity) {
                let track_tan = rail.tangent_at(train.pos.t);
                let track_dir = track_tan.normalize();
                train.dir = train.dir.project_on(track_dir);
                phy.vel = phy.vel.project_on(track_dir);
                let vel = phy.vel.dot(track_dir);
                train.pos.t += vel;
                let exit_info = if train.pos.t > rail.dist {
                    Some((rail.exit_dir(), rail.bezier.end, rail.end_junction))
                } else if train.pos.t < 0. {
                    Some((-rail.entry_dir(), rail.bezier.start, rail.start_junction))
                } else {
                    None
                };
                if let Some((exit_dir, exit_point, exit_junction)) = exit_info {
                    let junc = junctions.get(exit_junction).expect("next junction does not exist");
                    log::trace!("train {}: leaving junction {:?}", train.id, junc.name);
                    let mut next: Option<(TrackPos, Vector3<f32>)> = None;
                    let mut lowest_det: f32 = 999.;
                    let mut used_plan = false;
                    for exit in &junc.exits {
                        if exit.next.entity == train.pos.entity {
                            continue;
                        }
                        let entry_dir = exit.entry_dir;
                        let dot = exit_dir.dot(entry_dir);
                        let det = exit_dir.x * entry_dir.y - exit_dir.y * entry_dir.x;
                        if dot < MIN_TURN_DOT {
                            continue;
                        }
                        if let Some(&plan_ent) = train.plan.front() {
                            if plan_ent == exit.next.entity {
                                next = Some((exit.next, entry_dir));
                                used_plan = true;
                                break;
                            }
                        }
                        if det.abs() <= lowest_det.abs() {
                            next = Some((exit.next, entry_dir));
                            lowest_det = det;
                        }
                    }
                    if used_plan {
                        train.plan.pop_front();
                    } else {
                        log::info!("train {}: had to clear my plan because i've entered an unexpected rail piece: {:?}", train.id, next);
                        train.plan.clear();
                    }
                    if let Some((next_pos, entry_dir)) = next {
                        log::trace!("train {}: entering {:?} from {:?}", train.id, next_pos, entry_dir);
                        rail.reserved = None;
                        let next_rail = rails.get(next_pos.entity).expect("next rail piece does not exist");
                        // TODO: apply remaining force
                        train.pos = next_pos;
                        train.dir = entry_dir;
                    } else {
                        phy.vel = -phy.vel;
                        train.dir = -train.dir;
                        log::error!("train {}: no next position!!", train.id);
                        log::trace!("train {}: coming from direction {:?}", train.id, exit_dir);
                        for exit in &junc.exits {
                            if exit.next.entity == train.pos.entity {
                                continue;
                            }
                            log::trace!("train {}: there is an exit in direction {:?}", train.id, exit.entry_dir);
                        }
                    }
                }
            }
            if let Some(rail) = rails.get(train.pos.entity) {
                // makes the borrow checker a bit happier
                let new_pos = rail.point_at(train.pos.t);
                if new_pos.distance(phy.pos) > 0.1 {
                    train.recent.push_front(phy.pos);
                    train.recent.truncate(300);
                }
                phy.pos = new_pos;
            }
        }
    }
}
