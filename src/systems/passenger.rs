use {cgmath::prelude::*, specs::prelude::*};

use crate::{
    components::{Physical, Station, Train, TrainAi},
    linkgraph::LinkGraph,
    schedule::ScheduleDirectory,
};

pub struct PassengerSystem;

impl<'a> System<'a> for PassengerSystem {
    type SystemData = (
        Read<'a, LinkGraph>,
        Read<'a, ScheduleDirectory>,
        WriteStorage<'a, Station>,
        ReadStorage<'a, Physical>,
        WriteStorage<'a, Train>,
        ReadStorage<'a, TrainAi>,
    );

    fn run(&mut self, (lg, sd, mut stations, phys, mut trains, ais): Self::SystemData) {
        for (train, ai, phy) in (&mut trains, &ais, &phys).join() {
            if train.closed || phy.vel.magnitude() > 0.1 {
                continue;
            }
            let ent = train.pos.entity;
            for wagon in &mut train.wagons {
                if let Some(passenger_idx) = wagon.passengers.iter().position(|p| p.dest == ent) {
                    wagon.passengers.remove(passenger_idx);
                } else if let Some(station) = stations.get_mut(ent) {
                    if let Some(ref schedule_name) = ai.schedule {
                        let schedule = &sd.schedules[schedule_name];
                        let stops: Vec<Entity> = schedule
                            .stops
                            .iter()
                            .skip(ai.schedule_idx)
                            .map(|s| lg.get_station(&s.name).expect("station does not exist"))
                            .collect();
                        if let Some(passenger_idx) = station
                            .passengers
                            .iter()
                            .position(|p| stops.iter().any(|&s| s == p.dest))
                        {
                            let passenger = station.passengers.remove(passenger_idx);
                            wagon.passengers.push(passenger);
                        }
                    }
                }
            }
        }
    }
}
