use {
    cgmath::{
        prelude::*,
        Vector3,
        Rad,
        Rotation3,
        Basis3,
    },
    specs::prelude::*,
};

use crate::{
    camera::{
        Camera,
    },
    components::{Physical, Train},
    input::{
        Keyboard,
        Cursor,
    },
};

pub struct CameraSystem;

impl<'a> System<'a> for CameraSystem {
    type SystemData = (
        Entities<'a>,
        Write<'a, Camera>,
        Read<'a, Keyboard>,
        Read<'a, Cursor>,
        ReadStorage<'a, Physical>,
        ReadStorage<'a, Train>,
    );

    fn run(&mut self, (ents, mut cam, kb, cs, phys, trains): Self::SystemData) {
        if cs.scroll != 0. {
            let old_off = cam.state.offset;
            cam.state.offset -= cs.scroll * old_off * 0.07;
        }
        let speed = 10.;
        let fwd = Vector3::new(-cam.state.offset.x, 0., -cam.state.offset.z).normalize();
        let lwd = Vector3::new(0., 1., 0.).cross(fwd).normalize();
        if kb.down.contains(&0x11) {
            // W
            let offset = speed * fwd;
            cam.state.focus += offset;
        }
        if kb.down.contains(&0x1E) {
            // A
            let offset = speed * lwd;
            cam.state.focus += offset;
        }
        if kb.down.contains(&0x1F) {
            // S
            let offset = speed * -fwd;
            cam.state.focus += offset;
        }
        if kb.down.contains(&0x20) {
            // D
            let offset = speed * -lwd;
            cam.state.focus += offset;
        }
        if kb.down.contains(&0x10) {
            // Q
            let rot: Basis3<f32> = Rotation3::from_angle_y(Rad(-0.1));
            cam.state.offset = rot.rotate_vector(cam.state.offset);
        }
        if kb.down.contains(&0x12) {
            // E
            let rot: Basis3<f32> = Rotation3::from_angle_y(Rad(0.1));
            cam.state.offset = rot.rotate_vector(cam.state.offset);
        }
        if kb.down.contains(&0x13) {
            // R
            let rot: Basis3<f32> = Rotation3::from_axis_angle(lwd, Rad(0.1));
            cam.state.offset = rot.rotate_vector(cam.state.offset);
        }
        if kb.down.contains(&0x21) {
            // F
            let rot: Basis3<f32> = Rotation3::from_axis_angle(lwd, Rad(-0.1));
            cam.state.offset = rot.rotate_vector(cam.state.offset);
        }
        for &scancode in &kb.pressed {
            if scancode == 0x14 {
                // T pressed
                let mut passed = false;
                for (ent, _) in (&ents, &trains).join() {
                    if passed {
                        cam.state.follow = Some(ent);
                        break;
                    }
                    if let Some(entity) = cam.state.follow {
                        if ent == entity {
                            passed = true;
                        }
                    } else {
                        passed = true;
                        cam.state.follow = Some(ent);
                        break;
                    }
                }
                if !passed {
                    cam.state.follow = None;
                }
            } else if scancode == 0x15 {
                // Y pressed
                cam.state.follow = None;
                log::info!("stopped following train");
            } else if scancode == 0x16 {
                // U pressed
                cam.state.eye_camera = !cam.state.eye_camera;
                log::info!("eye camera state: {}", cam.state.eye_camera);
            }
        }
        let mut orbital = true;
        if let Some(entity) = cam.state.follow {
            if let Some(phy) = phys.get(entity) {
                cam.state.focus = phy.pos;
                if cam.state.eye_camera {
                    cam.state.real_focus = cam.state.focus;
                    orbital = false;
                    if let Some(train) = trains.get(entity) {
                        cam.eye = phy.pos + Vector3::new(0., 2., 0.);
                        cam.dir = train.dir;
                        cam.up = Vector3::new(0., 1., 0.);
                    }
                }
            }
        }
        if orbital {
            cam.state.real_focus = 0.4 * cam.state.real_focus + 0.6 * cam.state.focus.to_vec();
            cam.eye = cam.state.real_focus + cam.state.offset;
            cam.dir = -cam.state.offset.normalize();
            cam.up = Vector3::new(0., 1., 0.);
        }
    }
}
