use specs::prelude::*;

use crate::time::Clock;

pub struct ClockSystem;

impl<'a> System<'a> for ClockSystem {
    type SystemData = (Write<'a, Clock>,);

    fn run(&mut self, (mut clock,): Self::SystemData) {
        clock.update();
    }
}
