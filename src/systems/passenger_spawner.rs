use {
    rand::{self, seq::SliceRandom},
    specs::prelude::*,
};

use crate::{components::Station, passenger::Passenger, linkgraph::LinkGraph};

pub struct PassengerSpawnerSystem;

impl<'a> System<'a> for PassengerSpawnerSystem {
    type SystemData = (Entities<'a>, Read<'a, LinkGraph>, WriteStorage<'a, Station>);

    fn run(&mut self, (ents, lg, mut stations): Self::SystemData) {
        let mut rng = rand::thread_rng();
        let station_entities: Vec<Entity> = lg
            .stations()
            .filter(|&e| stations.get(e).unwrap().name != "depot")
            .collect();
        for (ent, station) in (&ents, &mut stations).join() {
            if station.name == "depot" {
                continue;
            }
            let dest = *station_entities.choose(&mut rng).unwrap();
            if dest == ent {
                continue;
            }
            if station.passengers.len() < 100 {
                station.passengers.push(Passenger { at: ent, dest });
            }
        }
    }
}
