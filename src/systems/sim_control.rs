use specs::prelude::*;

use crate::{
    sim_control::SimControl,
    input::{
        Keyboard,
    },
};

pub struct SimControlSystem;

impl<'a> System<'a> for SimControlSystem {
    type SystemData = (
        Write<'a, SimControl>,
        Read<'a, Keyboard>,
    );

    fn run(&mut self, (mut sc, kb): Self::SystemData) {
        if sc.pause_next {
            sc.paused = true;
            sc.pause_next = false;
            log::info!("simulation automatically paused");
        }
        for &scancode in &kb.pressed {
            if scancode == 0x39 {
                // space bar pressed
                sc.paused = !sc.paused;
                sc.pause_next = false;
                if sc.paused {
                    log::info!("simulation paused");
                }
                else {
                    log::info!("simulation resumed");
                }
            }
            else if scancode == 0x22 {
                // G pressed
                sc.paused = false;
                sc.pause_next = true;
                log::info!("simulation stepping for 1 frame");
            }
        }
    }
}
