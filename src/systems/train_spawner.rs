use {
    cgmath::{
        prelude::*,
        Vector3,
    },
    specs::prelude::*,
};

use crate::{
    ai::TrainAiState,
    components::{Physical, Rail, Train, TrainAi, Station},
    linkgraph::LinkGraph,
    schedule::ScheduleDirectory,
    time::{Clock, Span},
    track::TrackPos,
    train_numberer::TrainNumberer,
};

pub struct TrainSpawnerSystem;

impl<'a> System<'a> for TrainSpawnerSystem {
    type SystemData = (
        Entities<'a>,
        Read<'a, Clock>,
        Write<'a, TrainNumberer>,
        Write<'a, ScheduleDirectory>,
        Read<'a, LinkGraph>,
        WriteStorage<'a, Rail>,
        ReadStorage<'a, Station>,
        Read<'a, LazyUpdate>,
    );

    fn run(&mut self, (ents, clock, mut tn, mut sd, lg, mut rails, stations, lazy): Self::SystemData) {
        for schedule in sd.schedules.values_mut() {
            let depot = lg.get_station("depot").expect("did not find a station named \"depot\"");
            for slot in &mut schedule.slots {
                if slot.train.is_none() && slot.start - Span(1, 0) <= clock.now {
                    let station = stations.get(depot).unwrap();
                    for &rail_ent in &station.rails {
                        let rail = rails.get_mut(rail_ent).unwrap();
                        if rail.reserved.is_some() {
                            continue;
                        }
                        let mut ai = TrainAi::new();
                        ai.state = TrainAiState::Moving;
                        ai.schedule = Some(schedule.name.to_owned());
                        ai.slot = Some(slot.id);
                        let track_pos = TrackPos::new(rail_ent, rail.dist * 0.5);
                        let pos = rail.point_at(track_pos.t);
                        let dir = rail.tangent_at(track_pos.t);
                        let train_id = tn.new_id();
                        log::info!("spawning train {} from depot at {:?}", train_id, pos);
                        log::trace!("train {} spawn info: {:?} {:?} {:?}", train_id, track_pos, pos, dir);
                        let ent = lazy
                            .create_entity(&ents)
                            .with(Train::new(
                                train_id,
                                track_pos,
                                dir,
                                schedule.wagons,
                                10,
                            ))
                            .with(Physical::new(
                                pos,
                                Vector3::zero(),
                            ))
                            .with(ai)
                            .build();
                        slot.train = Some(ent);
                        rail.reserved = Some(ent);
                        break;
                    }
                }
            }
        }
    }
}
