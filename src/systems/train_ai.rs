use {cgmath::prelude::*, specs::prelude::*};

use crate::{
    ai::TrainAiState,
    components::{Physical, Rail, Station, Train, TrainAi, HasStation},
    linkgraph::LinkGraph,
    schedule::ScheduleDirectory,
    time::Clock,
};

pub struct TrainAiSystem;

impl<'a> System<'a> for TrainAiSystem {
    type SystemData = (
        Entities<'a>,
        Read<'a, Clock>,
        Read<'a, LinkGraph>,
        Read<'a, ScheduleDirectory>,
        WriteStorage<'a, Rail>,
        ReadStorage<'a, HasStation>,
        ReadStorage<'a, Station>,
        ReadStorage<'a, Physical>,
        WriteStorage<'a, Train>,
        WriteStorage<'a, TrainAi>,
    );

    fn run(
        &mut self,
        (ents, clock, lg, sd, mut rails, hss, stations, phys, mut trains, mut ais): Self::SystemData,
    ) {
        for (ent, train, phy, ai) in (&ents, &mut trains, &phys, &mut ais).join() {
            if let Some(ref schedule_name) = ai.schedule {
                let schedule = &sd.schedules[schedule_name];
                let schedule_item = &schedule.stops[ai.schedule_idx];
                let station_ent = lg.get_station(&schedule_item.name).expect("train ai can't find station");
                let station = stations.get(station_ent).unwrap();
                match ai.state {
                    TrainAiState::Arriving => {
                        if !train.plan.is_empty() {
                            log::warn!("train {}: arriving with a non-empty plan: {:?}", train.id, train.plan);
                        }
                        if let Some(rail) = rails.get(train.pos.entity) {
                            let dir = rail.tangent_at(train.pos.t).normalize();
                            let dot = dir.dot(train.dir);
                            let t = if dot > 0. {
                                train.pos.t
                            } else {
                                rail.dist - train.pos.t
                            };
                            if t < rail.dist * 0.5 {
                                train.throttle = 0.6;
                                train.brakes = false;
                            }
                            else if t < rail.dist * 0.6 {
                                train.throttle = 0.3;
                                train.brakes = true;
                            }
                            else {
                                train.throttle = 0.;
                                train.brakes = true;
                            }
                        }
                        else {
                            train.throttle = 0.;
                            train.brakes = true;
                        }
                        train.closed = true;
                        if phy.vel.magnitude() < 0.1 {
                            log::info!("train {}: arrived!", train.id);
                            ai.state = TrainAiState::Idle;
                        }
                    }
                    TrainAiState::Idle => {
                        train.throttle = 0.;
                        train.brakes = true;
                        train.closed = false;
                        if train
                            .wagons
                            .iter()
                            .all(|w| w.passengers.iter().all(|p| p.dest != station_ent))
                        {
                            if let Some(slot_id) = ai.slot {
                                let slot = &schedule.slots[slot_id];
                                let depart = slot.start + schedule_item.offset;
                                if depart <= clock.now {
                                    ai.schedule_idx += 1;
                                    if ai.schedule_idx >= schedule.stops.len() {
                                        ai.schedule_idx = 0;
                                        ai.schedule = None;
                                        ai.slot = None;
                                    } else {
                                        log::info!("train {}: started moving", train.id);
                                        ai.state = TrainAiState::Moving;
                                    }
                                }
                            }
                        }
                        if ai.schedule.is_some() && station.rails.iter().all(|&r| r != train.pos.entity) {
                            log::info!("train {}: could not stop on time!", train.id);
                            ai.state = TrainAiState::Moving;
                        }
                    }
                    TrainAiState::Moving => {
                        if train.plan.is_empty() {
                            log::info!("train {}: i have no plan on how to get to my destination", train.id);
                            let is_goal = |entity: Entity| -> bool {
                                // TODO: can surely be done more efficiently
                                if let Some(hs) = hss.get(entity) {
                                    if let Some(station) = stations.get(hs.station) {
                                        station.name == schedule_item.name
                                    } else {
                                        false
                                    }
                                } else {
                                    false
                                }
                            };
                            let is_reserved = |entity: Entity| -> bool {
                                let rail = rails.get(entity).unwrap();
                                rail.reserved.is_some() && rail.reserved != Some(ent)
                            };
                            let get_penalty = |entity: Entity| -> f32 {
                                if let Some(hs) = hss.get(entity) {
                                    let station = stations.get(hs.station).unwrap();
                                    if station.name == schedule_item.name {
                                        0.
                                    } else {
                                        300.
                                    }
                                } else {
                                    0.
                                }
                            };
                            let rail = rails.get(train.pos.entity).unwrap();
                            if let Some(path) = lg.find_path(
                                &rail,
                                train.pos,
                                train.dir,
                                is_goal,
                                is_reserved,
                                get_penalty,
                            ) {
                                train.plan = path.into();
                                log::info!("train {}: found a route with {} track pieces to my destination", train.id, train.plan.len());
                                for &ent2 in &train.plan {
                                    let rail = rails.get_mut(ent2).unwrap();
                                    rail.reserved = Some(ent);
                                }
                            } else {
                                log::info!("train {}: can't find a route to my destination", train.id);
                            }
                        }
                        train.closed = true;
                        if train.plan.is_empty() {
                            log::info!("train {}: no plan! stopping", train.id);
                            let rail = rails.get(train.pos.entity).unwrap();
                            let dir = rail.tangent_at(train.pos.t).normalize();
                            let dot = dir.dot(train.dir);
                            let t = if dot > 0. {
                                train.pos.t
                            } else {
                                rail.dist - train.pos.t
                            };
                            if t < rail.dist * 0.5 {
                                train.throttle = 0.6;
                                train.brakes = false;
                            }
                            else if t < rail.dist * 0.6 {
                                train.throttle = 0.3;
                                train.brakes = true;
                            }
                            else {
                                train.throttle = 0.;
                                train.brakes = true;
                            }
                        } else {
                            let rail = rails.get(train.pos.entity).unwrap();
                            if rail.reserved.is_some() && rail.reserved != Some(ent) {
                                log::warn!("train {}: i'm on a track that's reserved for another train!", train.id);
                                train.brakes = true;
                                train.throttle = 0.;
                            }
                            else {
                                train.brakes = false;
                                train.throttle += 0.1;
                                if train.throttle > 1. {
                                    train.throttle = 1.;
                                }
                            }
                        }
                        if let Some(hs) = hss.get(train.pos.entity) {
                            if hs.station == station_ent {
                                let station = stations.get(hs.station).unwrap();
                                log::info!("train {}: arriving at station {}", train.id, station.name);
                                train.plan.clear();
                                ai.state = TrainAiState::Arriving;
                            }
                        }
                    }
                }
            }
        }
    }
}
