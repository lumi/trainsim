Trainsim (name subject to change)
=================================

## NOTICE

These docs are terribly out of date, I need to update them.

What's this?
------------

A small project for simulating trains on schedules.

This was made for fun, I don't expect anything serious to come out of this, but it's always great to share the source! :3

How do I run it?
----------------

Run `cargo run --release` in the repository root.

How do I control this?
----------------------

There are quite a few keybindings, I'll go through them:

 - `W` `A` `S` `D`: Move the viewport up/left/down/right.
 - `Q` `E`: Rotate the viewport ccw/cw.
 - `R` `F`: Zoom in/out.
 - `T`: Cycle through trains.
 - `Y`: Deselect current train.

Can I change the map and/or schedules?
--------------------------------------

Yes! The tiles are described in `tiles.txt`, the world is described in `world.txt` and the schedules are described in `schedules.txt`.

The syntax of these files is very similar, every line starts with a command, then a space, then a space-separated list of arguments.

Commands that are not recognized are ignored, blank lines are ignored.

I'll explain the structure of these files.

### `schedules.txt`

```
SCHEDULE schedule-name
COLOR r g b
EVERY 00:30
STOP stop-1 00:10
STOP stop-2 00:40
STOP stop-3 01:10
DONE
```

Every schedule starts with the `SCHEDULE` command followed by the name of the schedule, this name cannot have any spaces in it, and ends with the `DONE` command.

Inside a schedule you can use:

 - `COLOR r g b` where `r`, `g` and `b` are values from 0 to 255 determining the color of the trains on the schedule.
 - `EVERY separation` determines at what frequency trains should depart at the first stop.
 - `STOP stop-name offset` adds a stop to the list of stops.
   - `stop-name` is the name of the stop.
   - `offset` is the time from the start of the timetable that it should depart from this stop.

### `world.txt`

```
TILE x y orientation tile-type [stop-name r g b]
```

A world definition only has one type of command, the `TILE` command, this command places a tile of the named type at (x, y) with an orientation from 0 to 3.

The rest is optional and only specified if this tile is a station. If it is, you add the name of a stop and the color of the stop. `r g b` are defined similarly as in `COLOR` in `schedules.txt`.

### `tiles.txt`

```
TILE tile-type
SIZE width height
TRACK start_x start_y end_x end_y
ARC center_x center_y radius start_angle end_angle
DONE
```

This file contains multiple tile definitions, which are used in `world.txt`. Every tile definition starts with `TILE tile-type`, which works analogously to `SCHEDULE schedule-name`. It also ends with `DONE`.

The coordinates here work differently from the tile coordinates, namely one unit is half a tile, this makes tiles easier to define as you don't have to use decimal values nearly as much. The same is done for the angles, one angular unit is 45 degrees.

As for the commands within a tile definition:

 - `SIZE width height` defines the size of the tile as (`width`, `height`), in tile units, not half tile units.
 - `TRACK start_x start_y end_x end_y` defines a track from (`start_x`, `start_y`) to (`end_x`, `end_y`).
 - `ARC center_x center_y radius start_angle end_angle` defines a track shaped as a circle arc with center point (`center_x`, `center_y`), radius `radius` and an angle from `start_angle` to `end_angle`. Angles start from the positive x axis, positive angles move clockwise.
 - `EARC center_x center_y radius_x radius_y start_angle end_angle` defines a track shaped as an ellipse arc with center point (`center_x`, `center_y`), radii (`radius_x`, `radius_y`) and an angle from `start_angle` to `end_angle`, like in `ARC`.

What license is it under?
-------------------------

AGPL-3.0
