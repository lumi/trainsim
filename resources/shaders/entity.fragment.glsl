#version 140

uniform vec4 default_color;
uniform sampler2D tex;

in vec4 v_color;
in vec2 v_uv;
in vec3 v_normal;

out vec4 color;

void main() {
    vec3 light_vec = normalize(vec3(1., 1., 1.));
    float light = 0.6 + dot(light_vec, v_normal) * 0.4;
    vec4 face_color = v_color; // texture(tex, v_uv);
    vec4 screen_color = mix(default_color, vec4(face_color.xyz, 1.), face_color.w);
    color = mix(vec4(0., 0., 0., 1.), screen_color, light);
}
