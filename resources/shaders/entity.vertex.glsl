#version 140

uniform mat4 model;
uniform mat4 view;
uniform mat4 perspective;

in vec3 position;
in vec2 uv;
in vec3 normal;
in vec4 color;

out vec4 v_color;
out vec2 v_uv;
out vec3 v_normal;

void main() {
    gl_Position = perspective * view * model * vec4(position, 1.);
    v_color = color;
    v_uv = uv;
    v_normal = normal;
}
