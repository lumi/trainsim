#version 140

uniform mat4 model;
uniform mat4 view;
uniform mat4 perspective;
uniform vec4 side_color;

in vec3 position;
in vec2 uv;
in vec3 normal;
in vec4 color;

out vec4 v_color;

void main() {
    vec3 light_vec = normalize(vec3(1., 1., 1.));
    gl_Position = perspective * view * model * vec4(position, 1.);
    vec4 terrain_color;
    float along = dot(normal, vec3(0., 1., 0.));
    if (along > 0.9) {
        terrain_color = color;
    }
    else if (along > 0.5) {
        terrain_color = mix(side_color, color, (along - 0.5) / .4);
    }
    else {
        terrain_color = side_color;
    }
    float light = 0.6 + dot(light_vec, normal) * 0.4;
    v_color = mix(vec4(0., 0., 0., 1.), terrain_color, light);
}
